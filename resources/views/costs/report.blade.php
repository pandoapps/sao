@extends('layouts.master')

@section('specific_styles')
<link rel="stylesheet" href="/assets/libs/buttons/buttons.dataTables.min.css">
@endsection

@section('content')
	<div class="container-fluid">
		<div class="box box-primary">
			<!-- Fomulário INICIO -->
			{!! Form::model(new \App\Apointment, ['method' => 'POST', 'route' => ['cost.relatorio']]) !!}
			<div class="box-header with-border">
				<div class="box-body" style="float: left; width: 50%; padding: 20px">
					<!-- Projeto -->
					<div class="form-group">
						<label>Projeto</label>
						{!! Form::select('category_id', $categories, 
							(!empty($category_selected)) ? $category_selected : null, 
							['id' => 'category_select', 'class' => 'form-control select2', 'placeholder' => 'Todos']) !!}
					</div>

					<!-- Intervalo de tempo -->
					<div class="form-group">
						<label>Intervalo de data</label>
						<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
							<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
							<span>{{ isset($date_range) ? $date_range : ''}}</span> <b class="caret"></b>
							<input type="hidden" name="date_range" value="{{ isset($date_range) ? $date_range : ''}}">
						</div>
					</div>			
				</div>

				<div class="box-body" style="padding: 20px; float: right; width: 50%;">				
					<!-- Tipo de gasto -->
					<div class="form-group">
						<label>Tipo de gasto</label>
						{!! Form::select('cost_type', $typesCost, 
							(!empty($cost_selected)) ? $cost_selected : '', 
							['id' => 'cost_select', 'class' => 'form-control select2', 'placeholder' => 'Todos']) !!}
					</div>

					<!-- Colaboradores -->
					<div class="form-group" id="usersName">
						<label for="usersName">Colaborador</label>
						{!! Form::select('user_id', $users, 
							(!empty($user_selected)) ? $user_selected : '', 
							['id' => 'user_select','class' => 'form-control select2', 'placeholder' => 'Todos']) !!}
					</div>		
				</div>
				<div style="text-align: center; width: 100%; overflow: auto">
					<button type="submit" class="btn btn-primary">Pesquisar</button>
				</div>
			</div>
			{!! Form::close() !!}
			<!-- Formulario FIM -->
		</div>

		<hr>
		
		<!-- Tabela de custos fixos e variáveis -->
		<!-- Esta seção aparece apenas se o custo selecionado for
		TODOS, FIXO ou VARIÁVEL -->
		@if ($cost_selected != 'payroll')
			<h3><strong>Custos de projetos</strong></h3>
			<div class="box box-primary">
				<div class="box-body">
					<table id="report-table" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Descrição</th>
								<th>Origem</th>
								<th>Tipo</th>
								<th>Valor</th>
							</tr>
						</thead>

						<tbody>
						@if(isset($tableDataCost))
							@foreach($tableDataCost as $td)
								<tr>
									<td>{{$td['name']}}</td>
									<td>{{$td['description']}}</td>
									<td>{{$td['origin']}}</td>
									<td>{{$td['type']}}</td>
									<td width="20%">R$ {{$td['value']}}</td>
								</tr>
							@endforeach
						@endif
						</tbody>

						<tfoot>
							<tr>
								<th colspan="4" style="text-align:right">Total da página:</th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		@endif

		<hr>

		<!-- Tabela de folha de pagamento -->
		<!-- Esta seção aparece apenas se o custo selecionado for
		TODOS ou FOLHA DE PAGAMENTO -->
		@if ($cost_selected == '' || $cost_selected == 'payroll')
			<h3><strong>Folha de Pagamento</strong></h3>
			<div class="box box-primary">
				<div class="box-body">
					<table id="report-user-table" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="2%">	</th>
								<th>Nome</th>
								<th>Horas Trabalhadas</th>
								<th>Salário Variável</th>
								<th>Salário Fixo</th>
								<th width="20%">Total</th>
							</tr>
						</thead>

						<tbody>
						@if (isset($tableDataUserCost['users']))
							@foreach($tableDataUserCost['users'] as $key=>$user)
								<!-- Se há algum projeto selecionado, os gastos são mostrados de acordo com os valores do projeto 
								e apenas das pessoas que trabalharam nesse projeto no período dado -->
								@if(isset($user['categories'][$category_selected_name]))
									<tr>	
										<!-- Se o projeto selecionado não for TODOS, não aparece a opção de expansão de informações -->
										<td></td>
										<td> {{ $key }} </td>
										<td> {{ $user['categories'][$category_selected_name]['totalH'] }} hora(s) </td>
										<td> R$ {{ number_format($user['categories'][$category_selected_name]['var_pay'], 2, ',', '.') }} </td>
										<td> R$ {{ number_format($user['fixed_pay'], 2, ',', '.') }} </td>
										<td> R$ {{ number_format($user['fixed_pay'] + $user['categories'][$category_selected_name]['var_pay'], 2, ',', '.') }} </td>
																											
									</tr>
								@else
									@if ($category_selected == '')
										<tr>									
											<td class="categories-control" style="cursor: pointer"><i class="fa fa-level-down" aria-hidden="true"></i></td>
											<td> {{ $key }} </td>									
											<td> {{ $user['totalH'] }} hora(s) </td>
											<td> R$ {{ number_format($user['var_pay_total'], 2, ',', '.') }} </td>
											<td> R$ {{ number_format($user['fixed_pay'], 2, ',', '.') }} </td>
											<td> R$ {{ number_format($user['pay_total'], 2, ',', '.') }} </td>																												
										</tr>
									@endif
								@endif
							@endforeach
						@endif
						</tbody>

						<tfoot>
							<tr>
								<th colspan="5" style="text-align:right">Total da página:</th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>	
		@endif

		<!-- Tabela de custos da empresa -->
		<!-- Esta seção aparece apenas quando o projeto
		é diferente de TODOS -->
		@if ($category_selected != '')
			<div style="display: inline-block; width: 100%;">
				<h3>
					<div class="hint--top hint--large" aria-label="Custos da empresa, no período selecionado, proporcionais ao projeto." style="float:left;">
						<strong>Custos da Empresa</strong>
					</div>
					
					<div class="hint--top hint--large" aria-label="Custos totais da empresa no período selecionado." style="margin-right : 2%; float:right;">
						<strong>Total 
						@if ($cost_selected == '')
							(Fixo + Variável)
						@elseif ($cost_selected == 'fixed')
							(Fixo)
						@else
							(Variável)
						@endif
						:</strong> R$ {{ number_format($totalCompanyCosts, 2, ',', '.') }}
					</div>
					<br>
				</h3>					
			</div>
				
			<div class="box box-primary">
				<div class="box-body">
					<table id="report-company-table" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="2%">	</th>
								<th>Modalidade</th>
								<th>Impacto</th>
								<th width="20%">Total</th>
							</tr>
						</thead>

						<tbody>
						@if(isset($tableDataCompanyCostLinear))
							<tr>
								<!-- A ordem desse primeiro td importa -> "costs linear" -->
								<td class="costs linear" style="cursor: pointer"><i class="fa fa-level-down" aria-hidden="true"></i></td>
								<td>Linear</td>
								<td>
									@if (isset($tableDataCompanyCostLinear[0]['impact']))
										{{ number_format($tableDataCompanyCostLinear[0]['impact']*100, 2, ',', '.') }} %
									@else
										0 %	
									@endif
								</td>
								<td width="20%">R$ {{ number_format($totalCompanyLinear, 2, ',', '.') }}</td>
							</tr>
						@endif
						@if(isset($tableDataCompanyCostPropValue))
							<tr>
								<!-- A ordem desse primeiro td importa -> "costs propValue" -->								
								<td class="costs propValue" style="cursor: pointer"><i class="fa fa-level-down" aria-hidden="true"></i></td>
								<td>Proporcional ao custo do projeto</td>
								<td>
									@if (isset($tableDataCompanyCostPropValue[0]['impact']))
										{{ number_format($tableDataCompanyCostPropValue[0]['impact']*100, 2, ',', '.') }} %
									@else
										0 %	
									@endif
								</td>
								<td width="20%">R$ {{ number_format($totalCompanyPropValue, 2, ',', '.') }}</td>
							</tr>
						@endif
						@if(isset($tableDataCompanyCostPropHour))
							<tr>
								<!-- A ordem desse primeiro td importa -> "costs propHour" -->								
								<td class="costs propHour" style="cursor: pointer"><i class="fa fa-level-down" aria-hidden="true"></i></td>
								<td>Proporcional às horas trabalhadas no projeto</td>
								<td>
									@if (isset($tableDataCompanyCostPropHour[0]['impact']))
										{{ number_format($tableDataCompanyCostPropHour[0]['impact']*100, 2, ',', '.') }} %
									@else
										0 %
									@endif 
								</td>
								<td width="20%">R$ {{ number_format($totalCompanyPropHour, 2, ',', '.') }}</td>
							</tr>
						@endif
						@if(isset($tableDataCompanyCostPropDay))
							<tr>
								<!-- A ordem desse primeiro td importa -> "costs propDay" -->								
								<td class="costs propDay" style="cursor: pointer"><i class="fa fa-level-down" aria-hidden="true"></i></td>
								<td>Proporcional ao dias ativos do projeto no período</td>
								<td>
									@if (isset($tableDataCompanyCostPropDay[0]['impact']))
										{{ number_format($tableDataCompanyCostPropDay[0]['impact']*100, 2, ',', '.') }} %
									@else
										0 %
									@endif 
								</td>
								<td width="20%">R$ {{ number_format($totalCompanyPropDay, 2, ',', '.') }}</td>
							</tr>
						@endif
						</tbody>
					</table>
				</div>
			</div>
		@endif

		{{-- Highchart aqui! --}}
		<div class="box box-primary"><div id="chartContainer"></div></div>
		{{--Fim do Hightchart--}}
	</div>
@endsection

@section('specific_scripts')
	<script src="/assets/libs/moment.js"></script>
	<script src="/assets/libs/jszip.min.js"></script>
	<script src="/assets/libs/buttons/buttons.html5.min.js"></script>
	<script src="/assets/libs/buttons/dataTables.buttons.min.js"></script>
@endsection

@section('inline_scripts')
<script>
	// Tabela de custos
	$('#report-table').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
		},
		"dom": 'Bfrtip',
		"buttons": ['excelHtml5'],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;

			// Remove o formato monetário para o cálculo
			var intVal = function ( i ) {
				return typeof i === 'string' ?
					replaceAux(i)*1 :
					typeof i === 'number' ?
						i : 0;
			};				

			// Total de todas as páginas
			total = api
				.column( 4 )
				.data()
				.reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );

			// Total da página selecionada página
			pageTotal = api
				.column( 4, { page: 'current'} )
				.data()
				.reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );

			// Atualiza o footer
			$( api.column( 4 ).footer() ).html(
				formatCurrency(pageTotal) +' ( '+ formatCurrency(total) +' Total)'
			);
		}
	});

	// Tabela de folha de pagamento
	$('#report-user-table').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
		},
		"dom": 'Bfrtip',
		"buttons": ['excelHtml5'],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;

			// Remove o formato monetário para o cálculo
			var intVal = function ( i ) {
				return typeof i === 'string' ?
					replaceAux(i)*1 :
					typeof i === 'number' ?
						i : 0;
			};				

			// Total de todas as páginas
			total = api
				.column( 5 )
				.data()
				.reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );

			// Total da página selecionada página
			pageTotal = api
				.column( 5, { page: 'current'} )
				.data()
				.reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );

			// Atualiza o footer
			$( api.column( 5 ).footer() ).html(
				formatCurrency(pageTotal) +' ( '+ formatCurrency(total) +' Total)'					
			);
		}
	});

	// Tabela custos da empresa
	$('#report-company-table').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
		},
		"dom": 'Bfrtip',
		"buttons": ['excelHtml5']		
	});

	// Gráfico só é feito caso haja as informações necessárias
	@if (count($categoriesCost) != 0)
		// Variável vinda do controller, contendo os projetos com
		// suas informações
		var categoriesCost = {!! json_encode($categoriesCost) !!};
		
		// Variável auxiliar que organiza os dados para o plot
		var auxTotalFixed = [];

		// Variável auxiliar que organiza os dados para o plot
		var auxTotalVariable = [];

		// Variável auxiliar que organiza os dados para o plot
		var auxTotalPayroll = [];

		// Totais de custos
		var totalFixed = 0;
		var totalVariable = 0;
		var totalPayroll = 0;
		var total = 0;

		// Organização dos vetores de plot e cálculo dos custos totais
		// fixos, variáveis e folha de pagamento (com o intuito
		// de obtenção das porcentagens de participação de cada custo)
		$.each(categoriesCost, function(index, category) {
			auxTotalFixed.push(Number(category.totalFixed.toFixed(2)));
			auxTotalVariable.push(Number(category.totalVariable.toFixed(2)));
			auxTotalPayroll.push(Number(category.totalPayroll.toFixed(2)));
			totalFixed += category.totalFixed;
			totalVariable += category.totalVariable;
			totalPayroll += category.totalPayroll;
			total += category.totalFixed + category.totalVariable + category.totalPayroll;
		});

		// Cálculo das porcentagens
		totalFixed = Number(((totalFixed / total) * 100).toFixed(2));
		totalVariable = Number(((totalVariable / total) * 100).toFixed(2));
		totalPayroll = Number(((totalPayroll / total) * 100).toFixed(2));

		Highcharts.setOptions({
			lang: {
				thousandsSep: '.',
				decimalPoint: ','
			}	
		});
		Highcharts.chart('chartContainer', {
			title: {
				text: 'Movimentações financeiras no(s) projeto(s)'
			},
			xAxis: {
				categories: Object.keys(categoriesCost)
			},
			yAxis: {
				title: {
					text: 'Valores'
				},
				labels: {
					formatter: function() {
						return formatCurrency(this.value);
					}
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size: 10px"><b>{point.key}</b></span><br/>',
				pointFormat: '<span style="color:{point.color}">●</span> {series.name}: <b>R$ {point.y}</b>'
			},
			labels: {
				items: [{
					html: '<b>Total de gastos</b>',
					style: {
						left: '78x',
						top: '0px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			},
			series: [{
				type: 'column',
				name: 'Gasto Fixo',
				data: auxTotalFixed
			}, {
				type: 'column',
				name: 'Gasto Variável',
				data: auxTotalVariable
			}, {
				type: 'column',
				name: 'Folha de pagamento',
				data: auxTotalPayroll
			}, {
				type: 'pie',
				name: 'Total de gastos',
				tooltip: {
					headerFormat: '<span style="font-size: 10px"><b>{point.key}: </b></span>',
					pointFormat: '{point.percentage:.2f} %',//'<span style="color:{point.color}">●</span> {series.name}: <b>123</b>'
				},
				data: [{
					name: 'Gasto Fixo',
					y: totalFixed,
					color: Highcharts.getOptions().colors[0] // Cor de Gasto Fixo
				}, {
					name: 'Gasto Variável',
					y: totalVariable,
					color: Highcharts.getOptions().colors[1] // Cor de Gasto Variável
				}, {
					name: 'Folha de pagamento',
					y: totalPayroll,
					color: Highcharts.getOptions().colors[2] // Cor de Folha de Pagamento
				}],
				center: [100, 50],
				size: 100,
				showInLegend: false,
				dataLabels: {
					enabled: false,
				}
			}]
		});
	@endif

	// Select de intervalo de data
	$(function() {
		var start = moment().subtract(6, 'days');
		var end = moment();

		function cb(start, end) {
			$('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
			$('#reportrange input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		}

		$('#reportrange').daterangepicker({
			ranges: {
				'Hoje': [moment(), moment()],
				'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'7 dias antes': [moment().subtract(6, 'days'), moment()],
				'30 dias antes': [moment().subtract(29, 'days'), moment()],
				'Este mês': [moment().startOf('month'), moment().endOf('month')],
				'Último mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'Último ano': [moment().subtract(365, 'days'), moment()]
			},
			locale: {
				cancelLabel: 'Cancelar',
				applyLabel: 'OK',
				format: 'DD/MM/YYYY',
				fromLabel: 'Desde',
				toLabel: 'Até',
				customRangeLabel: 'Selecionar intervalo',
				daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
				monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
					'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro',
					'Dezembro']
			}
		}, cb);
		if($('#reportrange input').val() === '') {
			cb(start, end);
		}
	});

	// Controla a visibilidade do select de colaboradores
	$(function () {
		// Verifica o que está selecionado no select de tipo de custo e mostra
		// ou não o select de colaboradores
		if ($('#cost_select').val() != "payroll") {
			document.getElementById('usersName').style.visibility = "hidden";
		} else {
			document.getElementById('usersName').style.visibility = "visible";
		}
		
		// Se o tipo de custo é folha de pagamento, a caixa de seleção de colaboradores
		// é mostrada
		$('#cost_select').change(function (e) {
			value = $('#cost_select').val();				
			if(value != "payroll"){
				document.getElementById('usersName').style.visibility = "hidden";
			}
			else{
				document.getElementById('usersName').style.visibility = "visible";
			}		
		});					
	});	

	$(document).ready(function() {	
		// Todas as informações referentes à tabela de Folha de pagamento
		var tableCostUser = {!! json_encode($tableDataUserCost) !!};

		// Evento que exibi as informações específicas de projeto de cada pessoa
		$('#report-user-table tbody').on('click', 'td.categories-control', function () {
			var table = $('#report-user-table').DataTable();
			var tr = $(this).closest('tr');
			var row = table.row( tr );
	
			if ( row.child.isShown() ) {
				// Se os detalhes estão sendo exibidos, eles são escondidos					
				row.child.hide();

				// Muda o ícone quando os detalhes são escondidos
				table.cell($(this).closest('td')).data('<i class="fa fa-level-down" aria-hidden="true">');
				tr.removeClass('shown');
			}
			else {					
				// Exibe os detalhes de projeto do usuário em questão
				row.child( subSectionUserCost(tableCostUser.users[row.data()[1]].categories) ).show();

				// Muda o ícone quando os detalhes são mostrados
				table.cell($(this).closest('td')).data('<i class="fa fa-level-up" aria-hidden="true"></i>');
				tr.addClass('shown');
			}
		} );

		// Tabelas de custo de empresa nas 4 modalidades
		// (linear, proporcional-valor, proporcional-hora
		// proporcional-dia)
		var tableDataCompanyCostLinear = {!! json_encode($tableDataCompanyCostLinear) !!};
		var tableDataCompanyCostPropValue = {!! json_encode($tableDataCompanyCostPropValue) !!};
		var tableDataCompanyCostPropHour = {!! json_encode($tableDataCompanyCostPropHour) !!};
		var tableDataCompanyCostPropDay = {!! json_encode($tableDataCompanyCostPropDay) !!};

		// Evento que exibe as informações adicionais de custos de empresa
		// na modalidade linear
		$('#report-company-table tbody').on('click', 'td.costs', function () {
			var table = $('#report-company-table').DataTable();
			var tr = $(this).closest('tr');
			var row = table.row( tr );

			// Variável que recebe a modalidade que os custos serão submetidos
			var modality = $(tr).children()[0].className.split(' ')[1];
			if ( row.child.isShown() ) {
				// Se os detalhes estão sendo exibidos, eles são escondidos					
				row.child.hide();

				// Muda o ícone quando os detalhes são escondidos
				table.cell($(this).closest('td')).data('<i class="fa fa-level-down" aria-hidden="true">');
				tr.removeClass('shown');
			}
			else {					
				// Exibe os detalhes de projeto do usuário em questão
				if(modality == 'linear') {
					row.child( subSectionCompanyCost(tableDataCompanyCostLinear) ).show();
				}
				if(modality == 'propValue') {
					row.child( subSectionCompanyCost(tableDataCompanyCostPropValue) ).show();
				}
				if(modality == 'propHour') {
					row.child( subSectionCompanyCost(tableDataCompanyCostPropHour) ).show();
				}
				if(modality == 'propDay') {
					row.child( subSectionCompanyCost(tableDataCompanyCostPropDay) ).show();
				}				

				// Muda o ícone quando os detalhes são mostrados
				table.cell($(this).closest('td')).data('<i class="fa fa-level-up" aria-hidden="true"></i>');
				tr.addClass('shown');
			}
		} );

		// Evento que exibe as informações adicionais de custos de empresa
		// na modalidade proporcional-Valor
		$('#report-company-table tbody').on('click', 'td.costs-propValue', function () {
			var table = $('#report-company-table').DataTable();
			var tr = $(this).closest('tr');
			var row = table.row( tr );
	
			if ( row.child.isShown() ) {
				// Se os detalhes estão sendo exibidos, eles são escondidos					
				row.child.hide();

				// Muda o ícone quando os detalhes são escondidos
				table.cell($(this).closest('td')).data('<i class="fa fa-level-down" aria-hidden="true">');
				tr.removeClass('shown');
			}
			else {					
				// Exibe os detalhes de projeto do usuário em questão
				row.child( subSectionCompanyCost(tableDataCompanyCostLinear) ).show();

				// Muda o ícone quando os detalhes são mostrados
				table.cell($(this).closest('td')).data('<i class="fa fa-level-up" aria-hidden="true"></i>');
				tr.addClass('shown');
			}
		} );

	} );

	// Informações adicionais por projeto de cada pessoa
	function subSectionUserCost (categories) {
		// Variável que recebe o html  construído a partir de todos
		// os projetos da pessoa
		categoriesAux = '';
		for(var category in categories) { 
			categoriesAux = categoriesAux +
			'<tr>'+
				'<td>' + category + '</td>'+
				'<td>' + Number(categories[category].totalH).toLocaleString('pt-BR') + ' hora(s)</td>'+
				'<td>' + formatCurrency(categories[category].var_pay) + '</td>'+
			'</tr>';	
		}

		// Variável que contém o código html da tabela de projetos de cada pessoa
		categoriesTable = 
			'<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
				'<thead>'+
					'<tr>'+
						'<th>Projeto</th>'+
						'<th>Horas Trabalhadas</th>'+
						'<th>Salário Variável</th>'+
					'</tr>'+
				'</thead>'+
				'<tbody>'+						
					categoriesAux
				'</tbody>'+				
			'</table>';

		return categoriesTable;
	}	

	// Informações adicionais custos de empresa
	function subSectionCompanyCost (costs) {
		// Variável que recebe o html construído a partir de todos
		// os custos da empresa submetidos à modalidade linear
		costsAux = '';
		for(var i=0; i< costs.length; i++) { 
			costsAux = costsAux +
			'<tr>'+
				'<td>' + costs[i].name + '</td>'+
				'<td>' + costs[i].type + '</td>'+
				'<td>' + Number(costs[i].impact*100).toLocaleString('pt-BR') + ' %</td>'+
				'<td>' + formatCurrency(costs[i].value) + '</td>'+
			'</tr>';	
		}

		// Variável que contém o código html da tabela de custos da empresa
		// submetidos à modalidade linear
		costsTable = 
			'<table id="report-company-table-linear" repcellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
				'<thead>'+
					'<tr>'+
						'<th>Nome</th>'+
						'<th>Tipo</th>'+
						'<th>Impacto</th>'+
						'<th>Valor</th>'+						
					'</tr>'+
				'</thead>'+
				'<tbody>'+						
					costsAux
				'</tbody>'+				
			'</table>';

		 return costsTable;
	}	

	// Remove o formato monetário e troca ',' por '.',
	// para o correto cálculo
	function replaceAux(i) {
		i = i.replace(/[R$." "]/g, '')		
		return i.replace(/[,]/g, '.');
	}

	// Função para formatar um número para um valor financeiro
	function formatCurrency(number) {
		var aux = Number(number).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
		// Adiciona um espaço entre a unidade monetária e o valor
		aux = aux.substr(0,2) + ' ' + aux.substring(2, aux.length);
		return aux;
	}
</script>
@endsection