@extends('layouts.master')

@section('content')
<div class="container-fluid">

    <div style="display: block; height: 90px">
		<div style="float: left;"><h2>Gastos</h2></div>
		<div style="float: right;">
			<h2>
				<a class="fa fa-plus-square" role="button" href="{{ route('cost.create') }}"></a>
			</h2>
		</div>
    </div>
    
    <table id="cost-table" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Nome</th>
                <th>Descrição</th>
                <th>Origem</th>
                <th>Tipo</th>
                <th>Valor</th>
                <th>Início</th>
                <th>Fim</th>
                <th width="5%">Ações</th>
			</tr>
        </thead>
        <tbody>
            @foreach($costs as $cost)
                <tr>
                    <td>{{$cost->name}}</td>
                    <td>{{$cost->description}}</td>
                    <td>{{$cost->costable->name}}</td>
                    <td>{{$cost->type_name}}</td>
                    
                    <td>R$ {{$cost->value}}</td>
                    <td>{{$cost->formatted_start}}</td>
                    <td>{{$cost->formatted_end}}</td>
                    <td>
                        <a href="{{route('cost.edit', ['id' => $cost->id])}}" class="showBtn fa fa-pencil fa-2x" title="Editar"></a>
                        <a href="{{ route('cost.destroy', ['id' => $cost->id]) }}" class="showBtn fa fa-trash-o fa-2x"></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
	</table>

</div>
@endsection

@section('inline_scripts')
<script>
    $("#cost-table").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
        },
    });
</script>
@endsection

