@extends('layouts.master')

@section('specific_styles')
<link rel="stylesheet" href="/assets/libs/buttons/buttons.dataTables.min.css">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="box box-primary">
            <!-- Fomulário INICIO -->
            {!! Form::model(new \App\Apointment, ['method' => 'POST', 'route' => ['cost.relatorio']]) !!}
            <div class="box-header with-border">
                <div class="box-body" style="float: left; width: 50%; padding: 20px">
                    <!-- Projeto -->
                    <div class="form-group">
                        <label>Projeto</label>
                        {!! Form::select('category_id', $categories, (!empty($category_selected)) ? $category_selected : null, ['id' => 'category_select', 'class' => 'form-control select2', 'placeholder' => 'Todos']) !!}
                    </div>

                    <!-- Intervalo de tempo -->
                    <div class="form-group">
                        <label>Intervalo de data</label>
                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                            <span>{{ isset($date_range) ? $date_range : ''}}</span> <b class="caret"></b>
                            <input type="hidden" name="date_range" value="{{ isset($date_range) ? $date_range : ''}}">
                        </div>
                    </div>			
                </div>

                <div class="box-body" style="padding: 20px; float: right; width: 50%;">				
                    <!-- Tipo de gasto -->
                    <div class="form-group">
                        <label>Tipo de gasto</label>
                        {!! Form::select('cost_type', $typesCost, 
                            (!empty($cost_selected)) ? $cost_selected : '', 
                            ['id' => 'cost_select', 'class' => 'form-control select2', 'placeholder' => 'Todos']) !!}
                    </div>

                    <!-- Colaboradores -->
                    <div class="form-group" id="usersName">
                        <label for="usersName">Colaborador</label>
                        {!! Form::select('user_id', $users, 
                            (!empty($user_selected)) ? $user_selected : '', 
                            ['id' => 'user_select','class' => 'form-control select2', 'placeholder' => 'Todos']) !!}
                    </div>		
                </div>
                <div style="text-align: center; width: 100%; overflow: auto">
                    <button type="submit" class="btn btn-primary">Pesquisar</button>
                </div>
            </div>
            {!! Form::close() !!}
            <!-- Formulario FIM -->
        </div>
    </div>
@endsection
@section('inline_scripts')
<script>
    $(function() {
        var start = moment().subtract(6, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            $('#reportrange input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }

        $('#reportrange').daterangepicker({
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 dias antes': [moment().subtract(6, 'days'), moment()],
                '30 dias antes': [moment().subtract(29, 'days'), moment()],
                'Este mês': [moment().startOf('month'), moment().endOf('month')],
                'Último mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Último ano': [moment().subtract(365, 'days'), moment()]
            },
            locale: {
                cancelLabel: 'Cancelar',
                applyLabel: 'OK',
                format: 'DD/MM/YYYY',
                fromLabel: 'Desde',
                toLabel: 'Até',
                customRangeLabel: 'Selecionar intervalo',
                daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
                    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro',
                    'Dezembro']
            }
        }, cb);
        if($('#reportrange input').val() === '') {
            cb(start, end);
        }
    });

    $(function () {
        // Verifica o que está selecionado no select de tipo de custo e mostra
        // ou não o select de colaboradores
        if ($('#cost_select').val() != "payroll") {
            document.getElementById('usersName').style.visibility = "hidden";
        } else {
            document.getElementById('usersName').style.visibility = "visible";
        }
        
        // Se o tipo de custo é folha de pagamento, a caixa de seleção de colaboradores
        // é mostrada
        $('#cost_select').change(function (e) {
            value = $('#cost_select').val();				
            if(value != "payroll"){
                document.getElementById('usersName').style.visibility = "hidden";
            }
            else{
                document.getElementById('usersName').style.visibility = "visible";
            }		
        });					
    });	
</script>
@endsection