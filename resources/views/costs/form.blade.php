@extends('layouts.master')

@section('content')

{{--  Fix de bug no footer da box  --}}
<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}

	.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
		border: 1px solid #d2d6de;
		border-radius: 0;
		padding: 6px 12px;
		height: 34px;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
	@if ($errors->any())
		<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-ban"></i> Erro!</h4>
			<ul>{!! implode('', $errors->all('<li>:message</li>')) !!}</ul>
		</div>
	@endif
		<div class="box box-primary box-create">
			<div class="box-header with-border">
				@if(isset($cost))
					<h3 class="box-title">Editar Gasto</h3>
				@else
					<h3 class="box-title">Criar Gasto</h3>
				@endif
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			@if(isset($cost))
			{!! Form::open(['method' => 'PUT', 'route' => ['cost.update', $cost->id]]) !!}
			@else
			{!! Form::open(['method' => 'POST', 'route' => ['cost.store']]) !!}
			@endif

            <div class="box-body">

                <div class="form-group">
                    <label for="name">Nome</label>
					<input type="text" class="form-control" name="name" value="{{isset($cost) ? $cost->name : old('name')}}">
                </div>

				<div class="form-group">
                    <label for="description">Descição</label>
					<input type="text"  class="form-control" name="description" value="{{isset($cost) ? $cost->description : old('description')}}">
                </div>
				
				<div class="form-group">
                    <label for="value">Valor</label>
					<input type="decimal"  class="form-control currency" name="value" value="{{isset($cost) ? $cost->value : old('value')}}">
                </div>

				<!-- O custo pode ser fixo ou variável -->
				<div class="form-group">
                    <label for="cost_type">Tipo do custo</label>
					<select name="type" class="form-control">						
						<option value="variable" {{(Input::old("type") == "variable" ? "selected":"") }}>Variável</option>
						<option value="fixed" {{(Input::old("type") == "fixed" ? "selected":"") }}>Fixo</option>
					</select>
                </div>
				<div class="form-group">
                    <label for="costable_type">Origem</label>
					<select id='costable_type' name="costable_type" class="form-control" type="text">						
						<!-- <option id="op_category" value="App\Category">Projeto</option>
						<option id="op_company" value="App\Company">Empresa</option>	-->
						<option id="op_category" value="App\Category" {{(Input::old("costable_type") == "App\Category" ? "selected":"") }}>Projeto</option>
						<option id="op_company" value="App\Company" {{(Input::old("costable_type") == "App\Company" ? "selected":"") }}>Empresa</option>												
					</select>
                </div>

				<div class="form-group" id="projectName">
                    <label for="costable_id">Nome do projeto</label>
					<select name="costable_id" class="form-control">
						@foreach($categories as $id => $name)							
							<option value="{{$id}}" {{ (Input::old("costable_id") == $id ? "selected":"") }}>{{$name}}</option>
						@endforeach
					</select>
                </div>

				<div class="form-group">
					<label for="exampleInputStart">Início</label>
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input id="datepickerStart" type="text" name="start" class="form-control datepicker" value="{{(isset($cost)&&isset($cost->start)) ? $cost->start->format('d/m/Y H:i') : old('start')}}">
					</div>
				</div>


				<div class="form-group">
					<label for="exampleInputEnd">Fim</label>
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input id="datepickerEnd" type="text" name="end" class="form-control datepicker" value="{{(isset($cost)&&isset($cost->end)) ? $cost->end->format('d/m/Y H:i') : old('end')}}">
					</div>
				</div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

@section('inline_scripts')
<script>
	$(document).ready(function () {
		// Se a opção do form é editar, a variável $cost vem do
		// CostController.php
		@if (isset($cost))
			// Variável que auxilia na alteração do select
			var cost = {!! json_encode($cost) !!};
			// Quando está na opção de editar, verifica se o custo é de empresa  
			// e altera o select
			if(cost['costable_type'] == "App\\Company") {
				$('select[name=costable_type]').val("App\\Company").change();
			}
		@endif
		
		// Verifica o que está selecionado no select de origem de custo e mostra
		// ou não o select de projetos
		if ($('select[name=costable_type]').val() == "App\\Company") {
			document.getElementById('projectName').style.visibility = "hidden";
		} else {
			document.getElementById('projectName').style.visibility = "visible";
		}

		// Se o tipo de custo é empresa, a caixa de seleção de nome do projeto não
		// é mostrada
		$('select[name=costable_type]').change(function (e) {
			value = $('select[name=costable_type]').val();
			
			if(value == "App\\Company"){
				document.getElementById('projectName').style.visibility = "hidden";
			}
			else{
				document.getElementById('projectName').style.visibility = "visible";
			}		
		});


	});

	// Máscara de moeda para o campo valor
	$(function() {
		$(".currency").maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
	});
</script>
@endsection