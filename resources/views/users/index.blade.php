@extends('layouts.master')

@section('content')
<div class="container-fluid">

    <div style="display: block; height: 90px">
		<div style="float: left;"><h2>Usuários</h2></div>
		<div style="float: right;">
			<h2>
				<a class="fa fa-plus-square" role="button" href="{{ route('user.create') }}"></a>
			</h2>
		</div>
    </div>
    
    <table id="users-table" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Nome</th>
                <th>Telefone</th>
                <th>Cargo</th>
                <th>Email</th>
                <th>Perfil</th>
                <th width="10%">HH</th>                
                <th width="7%">CH</th>
                <th width="10%">Salário Fixo</th>
                <th width="5%">Ações</th>
			</tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{($user->phone != NULL)? $user->phone : 'N/A'}}</td>
                    <td>{{($user->role != NULL)? $user->role : 'N/A'}}</td>                    
                    <td>{{$user->email}}</td>
                    <td>{{($user->roles->count()>0)? $user->roles->first()->display_name : 'N/A'}}</td>
                    <td>R$ {{$user->HH}} / hora</td>
                    <td>{{$user->CH}} hora(s)</td>
                    <td>R$ {{$user->fixed_pay}}</td>
                    <td>

					
  

                        <a href="{{route('user.edit', ['id' => $user->id])}}" class="showBtn fa fa-pencil fa-2x" title="Editar"></a>
						@if($user->id != \Auth::user()->id)
                        <a data-url="{{route('user.destroy',['id' => $user->id])}}" class="showBtn fa fa-trash-o fa-2x delete" ></a>
						@endif
					</td>
                </tr>
            @endforeach
        </tbody>
	</table>

</div>
@endsection

@section('inline_scripts')
<script>
//excluir usuario

$(document).on('click', '.delete', function() {
			 var url = $(this).attr("data-url");
			swal({
				title: 'Tem certeza?',
				text: "Você quer remover esse usuario?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Excluir!',
				cancelButtonText: 'Cancelar'
			}).then(function () {

				$.ajax({
					headers: {
						'X-CSRF-Token': '{!! csrf_token() !!}'
					},
					type: 'POST',
					url: url,
					enctype: 'multipart/form-data',
					success: function(){
						
							swal(
								'Deletado!',
								'O usuario foi excluido com sucesso!.',
								'success'
								);
							// apointments_table.ajax.reload();
						 
					},
					error: function(){
						swal(
							'Cancelado',
							'O usuario não foi excluido!',
							'error'
							)
					}
				});	
			}, function (dismiss) {
					  // dismiss can be 'cancel', 'overlay',
					  // 'close', and 'timer'
					  if (dismiss === 'cancel') {
					  	swal(
					  		'Cancelado',
					  		'A tarefa não foi excluida',
					  		'error'
					  		)
					  }
					});

		});



    $("#users-table").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
        },
    });
</script>
@endsection

