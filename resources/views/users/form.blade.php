@extends('layouts.master')

@section('content')

{{--  Fix de bug no footer da box  --}}
<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
	@if ($errors->any())
		<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-ban"></i> Erro!</h4>
			<ul>{!! implode('', $errors->all('<li>:message</li>')) !!}</ul>
		</div>
	@endif
	<div class="box box-primary box-create">
			<div class="box-header with-border">
				@if(isset($user))
					<h3 class="box-title">Editar Usuário</h3>
				@else
					<h3 class="box-title">Criar Usuário</h3>
				@endif
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			@if(isset($user))
			{!! Form::open(['method' => 'PUT', 'route' => ['user.update', @$user->id]]) !!}
			@else
			{!! Form::open(['method' => 'POST', 'route' => ['user.store']]) !!}
			@endif

            <div class="box-body">

                <div class="form-group">
                    <label for="name">Nome*</label>
                    {!! Form::text('name', (isset($user) ? @$user->name : null), ['class' => 'form-control']) !!}
				</div>
				
				<!-- Telefone -->
				<div class="form-group">
                    <label for="phone">Telefone</label>
                    {!! Form::text('phone', (isset($user) ? @$user->phone : null), ['class' => 'form-control', 'attrname'=> 'telephone1']) !!}
				</div>

				<!-- Cargo -->
				<div class="form-group">
                    <label for="role">Cargo</label>
                    {!! Form::text('role', (isset($user) ? @$user->role : null), ['class' => 'form-control']) !!}
				</div>

                <div class="form-group">
                    <label>Email</label>
					@if(Auth::user()->roles->first()->name == 'admin')
						@if(Auth::user()->id == @$user->id)						
							{!! Form::text('email', (isset($user) ? @$user->email : null), ['class' => 'form-control']) !!}						
						@else
							{!! Form::email('email', (isset($user) ? @$user->email : null), ['class' => 'form-control']) !!}						
						@endif
					@else
						{!! Form::email('email', (isset($user) ? @$user->email : null), ['class' => 'form-control']) !!}								
					@endif                    
                </div>

                <div class="form-group">
                    <label>Senha</label>
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="name">Carga Horária</label>
                    {!! Form::text('CH', (isset($user) ? @$user->CH : null), ['class' => 'form-control']) !!}
                </div>

				<div class="form-group">
                    <label for="HH">Valor HH</label>
                    {!! Form::text('HH', (isset($user) ? @$user->HH : null), ['class' => 'form-control ']) !!}
				</div>
				
				<!-- Campo de salário fixo -->
				<div class="form-group">
                    <label >Salário Fixo</label>
                    {!! Form::text('fixed_pay', (isset($user) ? @$user->fixed_pay : null), ['class' => 'form-control ']) !!}
				</div>

				<div class="form-group">
                    <label for="option">Horários complementados?</label>
					<label>
						{!! Form::radio('resposta', 'SIM') !!}
						Sim
					</label>
					<label>
						{!! Form::radio('resposta', 'NÃO') !!}
						Não
					</label>	
				</div>
				<!-- campo de projetos para usar horas restantes -->
				<div class="form-group" id="campo-escondido">
				<label for="projeto">Projeto para alocar horas: </label>
        		{!! Form::select('category_id', ['' => ''], '', ['id' => 'category_id_bar', 'class' => 'form-control select2 size-select', 'disabled' => 'disabled','placeholder' => 'Selecione um projeto...']) !!}
      			</div>

      			<div class="form-group">
	  			<label for="tarefa">Tarefa para alocar horas: </label>
       			 {!! Form::select('task_id', ['' => ''], '', ['id' => 'task_id_bar', 'class' => 'form-control select2 size-select', 'disabled' => 'disabled']) !!}
      			</div>


				<!-- Company Id Field -->
				<!-- 
					verificar se o user é admin, e se o id do usuario que esta sendo mudado
					é o mesmo do usuario logado
				-->
				@if(Auth::user()->roles->first()->name == 'admin')
					@if(Auth::user()->id != @$user->id)
						<div class="form-group">
							<label>Empresa</label>
							{!! Form::select('company_id', ['' => 'Selecionar'] + $companies, isset($user->company)? @$user->company->id : null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							<label>Perfil</label>
							{!! Form::select('role_id', ['' => 'Selecionar'] + $roles, isset($user->company)?(($user->roles->count()>0)? @$user->roles->first()->id : null):null, ['class' => 'form-control']) !!}
						</div>
					@endif
				@endif

				@if(Auth::user()->roles->first()->name == 'manager')
					@if(isset($user))
						{!! Form::hidden('company_id', @$user->company_id) !!}
					@else
						{!! Form::hidden('company_id', Auth::user()->company_id) !!}
					@endif
				
					<div class="form-group">
						<label>Perfil</label>
						{!! Form::select('role_id', ['' => 'Selecionar'] + $roles, isset($user->company)?((@$user->roles->count()>0)? @$user->roles->first()->id : null):null, ['class' => 'form-control']) !!}
					</div>
				@endif

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

@section('specific_scripts')
<script>
	// $(function() {
	// 	$(".currency").maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
	// })
	
	$("input[name=resposta]").change(function(data){ 
    var selection = $("input[name=resposta]:checked").val();
    console.log(selection); 
    if(selection != "SIM")
        $("#campo-escondido").hide();
    else
        $("#campo-escondido").show();
});

$("#campo-escondido").hide();

  	// Início máscara de telefone
  	function inputHandler(masks, max, event) {
		var c = event.target;
		var v = c.value.replace(/\D/g, '');
		var m = c.value.length > max ? 1 : 0;
		VMasker(c).unMask();
		VMasker(c).maskPattern(masks[m]);
		c.value = VMasker.toPattern(v, masks[m]);
	}

	var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
	var tel1 = document.querySelector('input[attrname=telephone1]');
	VMasker(tel1).maskPattern(telMask[0]);
	VMasker(tel1).maskPattern(telMask[1]);
	tel1.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);			
	// Fim máscara de telefone
</script>
@endsection