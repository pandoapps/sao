@extends('layouts.master')

@section('content')

<div class="container-fluid">

    <div style="display: block; height: 90px">
		<div style="float: left;"><h2>Empresas</h2></div>
		<div style="float: right;">
			<h2>
	
				<a class="fa fa-plus-square" role="button" href="{{ route('company.create') }}"></a>
			</h2>
		</div>
    </div>
    
    <table id="users-table" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Nome</th>
                <th>Email</th>
                <th>Endereço</th>
                <th>Telefone</th>
                <th>Validade</th>
				<th>Ações</th>

                
			</tr>
        </thead>
        <tbody>
            @foreach($companies as $company)
                <tr>
                    <td>{{$company->name}}</td>
                    <td>{{$company->email}}</td>
                    <td>{{$company->address}}</td>                    
                    <td>{{($company->phone != NULL)? $company->phone : 'N/A'}}</td>
                    <td>{{$company->expired_at}}</td>
                    
                    <td>
                        <a href="{{route('company.edit', ['id' => $company->id])}}" class="showBtn fa fa-pencil fa-2x" title="Editar"></a>
						@if($company->id != \Auth::user()->company_id)
                        <a data-url="{{route('company.destroy',['id' => $company->id])}}" class="showBtn fa fa-trash-o fa-2x delete" ></a>
						@endif
					</td>
                </tr>
            @endforeach
        </tbody>
	</table>

</div>
@endsection

@section('inline_scripts')
<script>
//excluir usuario

$(document).on('click', '.delete', function() {
			 var url = $(this).attr("data-url");
			swal({
				title: 'Tem certeza?',
				text: "Você quer remover esse usuario?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Excluir!',
				cancelButtonText: 'Cancelar'
			}).then(function () {

				$.ajax({
					headers: {
						'X-CSRF-Token': '{!! csrf_token() !!}'
					},
					type: 'POST',
					url: url,
					enctype: 'multipart/form-data',
					success: function(){
						
							swal(
								'Deletado!',
								'O usuario foi excluido com sucesso!.',
								'success'
								);
							// apointments_table.ajax.reload();
						 
					},
					error: function(){
						swal(
							'Cancelado',
							'O usuario não foi excluido!',
							'error'
							)
					}
				});	
			}, function (dismiss) {
					  // dismiss can be 'cancel', 'overlay',
					  // 'close', and 'timer'
					  if (dismiss === 'cancel') {
					  	swal(
					  		'Cancelado',
					  		'A tarefa não foi excluida',
					  		'error'
					  		)
					  }
					});

		});



    $("#users-table").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
        },
    });
</script>
@endsection

