@extends('layouts.master')

@section('content')

{{--  Fix de bug no footer da box  --}}
<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
		<div class="box box-primary box-create">
			<div class="box-header with-border">
				@if(isset($company))
					<h3 class="box-title">Editar Empresa</h3>
				@else
					<h3 class="box-title">Criar Empresa</h3>
				@endif
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			@if(isset($company))
			{!! Form::open(['method' => 'PUT', 'route' => ['company.update', $company->id]]) !!}
			@else
			{!! Form::open(['method' => 'POST', 'route' => ['company.store']]) !!}
			@endif

            <div class="box-body">

                <div class="form-group">
                    <label for="name">Nome*</label>
                    {!! Form::text('name', (isset($company) ? $company->name : null), ['class' => 'form-control']) !!}
				</div>
				
				<!-- Telefone -->
				<div class="form-group">
                    <label for="email">email</label>
                    {!! Form::text('email', (isset($company) ? $company->email : null), ['class' => 'form-control']) !!}
				</div>

				<!-- Cargo -->
				<div class="form-group">
                    <label for="address">Cargo</label>
                    {!! Form::text('address', (isset($company) ? $company->address : null), ['class' => 'form-control']) !!}
				</div>

                <div class="form-group">
                    <label for="phone">phone</label>
                    {!! Form::text('phone', (isset($company) ? $company->phone : null), ['class' => 'form-control']) !!}
                </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

@section('specific_scripts')
<script>
	// $(function() {
	// 	$(".currency").maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
	// })

  	// Início máscara de telefone
  	function inputHandler(masks, max, event) {
		var c = event.target;
		var v = c.value.replace(/\D/g, '');
		var m = c.value.length > max ? 1 : 0;
		VMasker(c).unMask();
		VMasker(c).maskPattern(masks[m]);
		c.value = VMasker.toPattern(v, masks[m]);
	}

	var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
	var tel1 = document.querySelector('input[attrname=telephone1]');
	VMasker(tel1).maskPattern(telMask[0]);
	VMasker(tel1).maskPattern(telMask[1]);
	tel1.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);			
	// Fim máscara de telefone
</script>
@endsection