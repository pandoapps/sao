<div class="box-body">

	<div class="form-group">
		<label>Deadline*</label>
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			{!! Form::text('deadline', isset($task) ? $task->deadline->format('d/m/Y H:i') : '', ['id' => 'datepicker', 'class' => 'form-control pull-right datepicker']); !!}
		</div>
	</div>

	<div class="form-group">
		<label>Projeto*</label>
		{!! Form::select('category_id', $categories, isset($task) ? $task->category_id : '', ['id' => 'category_select', 'class' => 'form-control select2', 'placeholder' => 'Selecione uma opção...']) !!}
	</div>

	<div class="form-group">
		<label>Nome*</label>
		{!! Form::text('name', null, ['id' => 'category_select', 'class' => 'form-control select2', 'placeholder' => 'Qual o nome da atividade?']); !!}
	</div>

	<div class="form-group">
		<label>Porcentagem</label>
		@if(isset($task))
			{!! Form::text('percentage', $task->percentage ,['class' => 'form-control'])!!}
		@else
		{!! Form::text('percentage', '' ,['class' => 'form-control', 'disabled' => true])!!}
		@endif
	</div>

	<div class="form-group">
		<label>Horas previstas</label>
		{!! Form::text('planned_hours', isset($task) ? $task->planned_hours : '', ['class' => 'form-control', 'placeholder' => 'Horas previstas para execução da tarefa']) !!}
	</div>

	<div class="form-group">
		<label>Descrição</label>
		{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Descrição da tarefa']) !!}
	</div>

	<div class="box-footer">
		<button type="submit" class="btn btn-primary">Salvar</button>
	</div>
</div>
@section('inline_scripts')
<script>
	$(function() {
		$('#nav-bar-ponto').html('');
	});
</script>
@endsection