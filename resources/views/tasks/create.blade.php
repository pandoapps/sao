@extends('layouts.master')

@section('content')
<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
		<div class="box box-primary box-create">
			<div class="box-header with-border">
				<h3 class="box-title">Criar Tarefas</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			{!! Form::model(new \App\Task, ['method' => 'POST', 'route' => ['task.store']]) !!}
			@include('tasks.partials._form')
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

@section('inline_scripts')

@endsection