<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISG - Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/adminlte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/adminlte/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/adminlte/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="/adminlte/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  {{-- <link rel="stylesheet" href="/adminlte/plugins/datepicker/datepicker3.css"> --}}
  <link rel="stylesheet" href="/assets/libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- SweetAlert -->
  <link rel="stylesheet" href="/assets/libs/sweetalert2/dist/sweetalert2.css" media="all"/>

  <!-- Select2 -->
  {{-- <link rel="stylesheet" href="/adminlte/plugins/select2/select2.min.css"> --}}
  <link rel="stylesheet" href="/assets/libs/select2/dist/css/select2.min.css" />
  <link rel="stylesheet" href="/assets/libs/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="/assets/css/custom.css">

  <!-- Styles -->
  @section('specific_styles')
  @show

<style>
    .box_login {
      width: 400px;
      margin: auto;
    }

    .box_login input {
      margin: 15px 0px;
    }

    html, body {
      height: 100%;
      min-height: 100%;
    }

    .wrapper {
      height: 100%;
      min-height: 100%;
      display: -webkit-flex;
      display: flex;
      -webkit-align-items: center;
      align-items: center;
      -webkit-justify-content: center;
      justify-content: center;
    }
  }

</style>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="">
  <div class="wrapper">
    <div class="box box-solid box-primary box_login">
      <div class="box-header">
        <h3 class="box-title">Login</h3>
      </div>
      <div class="box-body">
        <form action="{{ route('login') }}" method="post" name="login">
          {!! csrf_field() !!}
          <input type="text" name="email" class="form-control" id="navbar-search-input" placeholder="email" />
          <input type="password" name="password" class="form-control" id="navbar-search-input" placeholder="senha" />
          <center>
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary btn-flat">Login</button>
            </span>
          </center>
        </form>
      </div>
    </div>
  </div>
</body>
</html>