@extends('layouts.master')

@section('content')

<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
		@if (session('error'))
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Erro!</h4>
				{{ session('error') }}
			</div>
		@endif
		<div class="box box-primary box-create">
			<div class="box-header with-border">
				<h3 class="box-title">Criar Apontamento</h3>
			</div>
						
			<!-- /.box-header -->
			<!-- form start -->
			{!! Form::open(['method' => 'POST', 'route' => ['apointment.store']]) !!}
			@include('apointments.partials._form')
			{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection