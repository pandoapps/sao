@extends('layouts.master')

@section('specific_styles')
	<link rel="stylesheet" href="/assets/libs/buttons/buttons.dataTables.min.css">
@endsection

@section('content')

<div class="container-fluid">
	<div class="box box-primary">
		<!-- Fomulario INICIO -->
		{!! Form::model(new \App\Apointment, ['method' => 'POST', 'route' => ['apointment.relatorio','colaboradores']]) !!}
		<div class="box-header with-border">
			<div class="box-body" style="float: left; width: 50%; padding: 20px">
				<div class="form-group">
					<label>Colaborador</label>
					{!! Form::select('user_id', [null=>'Todos'] +  $users, $user_selected, ['id' => 'user_select',
					'class' => 'form-control select2', 'placeholder' => '']) !!}
				</div>
				<div class="form-group">
					<label>Intervalo de data</label>
					<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
						<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
						<span>{{ isset($date_range) ? $date_range : ''}}</span> <b class="caret"></b>
						<input type="hidden" name="date_range" value="{{ isset($date_range) ? $date_range : ''}}">
					</div>
				</div>

			</div>
			<div class="box-body" style="padding: 20px; float: right; width: 50%;">
				<div class="form-group">
					<label>Projeto</label>
					{!! Form::select('category_id', $categories, (!empty($category_selected)) ? $category_selected : null, ['id' => 'category_select', 'class' => 'form-control select2', 'placeholder' => 'Todas']) !!}
				</div>
				<div class="form-group">
					<label>Tarefa</label>
					{!! Form::select('task_id', $tasks, (!empty($task_selected)) ? $task_selected : '', ['id' => 'task_select', 'id' => 'task_select', 'class' => 'form-control select2', 'placeholder' => 'Todas']) !!}
				</div>
			</div>
			<div style="text-align: center; width: 100%; overflow: auto">
				<button type="submit" class="btn btn-primary">Pesquisar</button>
			</div>
		</div>
		{!! Form::close() !!}
		<!-- Formulario FIM -->
	</div>

	{{-- Highchart aqui! --}}
	<div class="box box-primary"><div id="chartContainer"></div></div>
	{{--Fim do Hightchart--}}

	<div class="box box-primary">
		<div class="box-body">
			<table id="report-table" class="display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>Nome</th>
					<th>Projeto</th>
					<th>Tarefa</th>
					<th>Entrada</th>
					<th>Saída</th>
					<th>Duração</th>
					<th>Ult. Atualização</th>
				</tr>
				</thead>
				<tbody>
				@foreach($tableData as $td)
					<tr>
						<td>{{$td['nome']}}</td>
						<td>{{$td['categoria']}}</td>
						<td>{{$td['tarefa']}}</td>
						<td class="moment">{{$td['entrada']}}</td>
						<td class="moment">{{$td['saida']}}</td>
						<td>{{$td['duracao']}}</td>
						<td class="moment">{{$td['update']}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="box box-primary" style="padding: 10px">
		<div style="display: block;">Intervalo: {{ $range }} dias</div>
		<div style="display: block;">Colaboradores: </div>
		<div style="display: block;">
			Horas trabalhadas: {{ $total_worked_hours }}h{{ $remaining_minutes}}min</div>
		</div>

	</div>
	@endsection

	@section('specific_scripts')
		<script src="/assets/libs/moment.js"></script>
		<script src="/assets/libs/jszip.min.js"></script>
		<script src="/assets/libs/buttons/buttons.html5.min.js"></script>
		<script src="/assets/libs/buttons/dataTables.buttons.min.js"></script>
	@endsection

	@section('inline_scripts')
	<script>
		$('#report-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
            },
            "dom": 'Bfrtip',
			"buttons": ['excelHtml5'],
			"order": [[ 4, "desc" ]]			
		});	

//		$('.moment').each(function(){
//		    var val = $(this).text();
//            $(this).text(moment(v	al).format('DD/MM/YYYY - hh:mm:ss'));
//		});

		$(function() {
			var start = moment().subtract(6, 'days');
			var end = moment();

			function cb(start, end) {
				$('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
				$('#reportrange input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
			}

			$('#reportrange').daterangepicker({
				ranges: {
					'Hoje': [moment(), moment()],
					'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'7 dias antes': [moment().subtract(6, 'days'), moment()],
					'30 dias antes': [moment().subtract(29, 'days'), moment()],
					'Este mês': [moment().startOf('month'), moment().endOf('month')],
					'Último mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
					'Último ano': [moment().subtract(365, 'days'), moment()]
				},
				locale: {
					cancelLabel: 'Cancelar',
					applyLabel: 'OK',
					format: 'DD/MM/YYYY',
					fromLabel: 'Desde',
					toLabel: 'Até',
					customRangeLabel: 'Selecionar intervalo',
					daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
					monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
						'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro',
						'Dezembro']
				}
			}, cb);
			if($('#reportrange input').val() === '') {
				cb(start, end);
			}
		});

		$(function () {
			Highcharts.chart('chartContainer', {
				exporting: {
					enabled: true
				},
				chart: {
					type: 'column',
					options3d: {
						enabled: false,
						alpha: 0,
						beta: 25,
						depth: 70
					}
				},
				title: {
					text: 'Horas trabalhadas x dias'
				},
				subtitle: {
					text: 'Colaborador: '
				},
				plotOptions: {
					column: {
						depth: 25
					}
				},
				xAxis: {
					categories: {!! $xAxis !!},
					title: {
						text: 'dias'
					}
				},
				yAxis: {
					title: {
						text: 'Horas trabalhadas'
					}
				},
				series: [{
						name: 'Horas',
						data: {!! $chart1 !!},
					},
					{
						type: 'spline',
						name: 'Carga horária esperada',
						data: {!! $line !!},
						marker: {
							lineWidth: 2,
							lineColor: Highcharts.getOptions().colors[3],
							fillColor: 'white'
						}
					}
				]
			});
		});
	</script>

	@endsection