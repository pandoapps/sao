<div class="box-body">
	<div class="form-group">
		<label>Projeto</label>
		{!! Form::select('category_id', $categories, isset($apointment) ? $apointment->category_id : '', ['id' => 'category_select', 'class' => 'form-control select2', 'placeholder' => 'Selecione uma opção...']) !!}
	</div>
	<div class="form-group">
		
	</div>
	<div class="form-group">
		<label>Tarefa</label>
		{!! Form::select('task_id', !empty($tasks) ? $tasks : ['' => 'Selecione uma tarefa antes...'], 
		isset($apointment) ? $apointment->task_id : '', ['id' => 'task_select', 'class' => 'form-control select2', 'placeholder' => 'Selecione uma tarefa antes...', 'disabled' => 'disabled']) !!}
	</div>
	<div class="form-group">
		<label for="exampleInputStart">Data/hora Inicio</label>
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			{!! Form::text('start', isset($apointment) ? $apointment->start->format('d/m/Y H:i') : '', ['id' => 'datepickerStart', 'class' => 'form-control pull-right datepicker']) !!}
		</div>
	</div>
	<div class="form-group">
		<label for="exampleInputStart">Data/hora Fim</label>
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			{!! Form::text('end', (isset($apointment) && $apointment->end != null) ? $apointment->end->format('d/m/Y H:i') : '', ['id' => 'datepickerEnd', 'class' => 'form-control pull-right datepicker']) !!}
		</div>
	</div>
	<div class="form-group">
		<label>Comentário</label>
		{!! Form::textarea('comment', null, ['id' => 'comment', 'class' => 'form-control', 'rows' => '4']) !!}
	</div>
</div>
<!-- /.box-body -->

<div class="box-footer">
	<button type="submit" class="btn btn-primary">Salvar</button>
</div>

@section('inline_scripts')
<script>
	$(function() {
		$('#nav-bar-ponto').html('');
	});
</script>
@endsection