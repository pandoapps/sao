@extends('layouts.master')

@section('content')
<div class="container-fluid">

    <div style="display: block; height: 90px">
		<div style="float: left;"><h2><strong>Projetos</strong></h2></div>
		<div style="float: right;">
			<h2>
				<a class="fa fa-plus-square" role="button" href="{{ route('category.create') }}"></a>
			</h2>
		</div>
    </div>
    
    @if( count($categories_for_table) != 0)
        <table id="category-table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th width="5%">Nº de tarefas</th>
                    <th width="10%">HH</th>
                    <th width="8%">Ações</th>
                </tr>
            </thead>
            <tbody>
                <!-- Categorias = Projetos -->
                @foreach($categories_for_table as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->tasksNumber }}</td>
                        <td>{{ $category->HH }}</td>                        
                        <td>                        
                            <a  href="{{ route('category.edit', ['id' => $category->id]) }}" 
                                class="showBtn fa fa-pencil fa-2x" 
                                title="Editar"></a>
                            <a  href="#" 
                                id_category="{{ $category->id }}"
                                class="checkBtn fa fa-check-square-o fa-2x finalizar-projeto" 
                                title="Finalizar"></a>
                            <a  href="{{ route('category.destroy', ['id' => $category->id]) }}" 
                                class="showBtn fa fa-trash-o fa-2x" 
                                title="Excluir"></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    @if( count($categories_for_finished_table) != 0)
        <!-- Divisão de projetos finalizados -->
        <div style="display: block; height: 90px">
            <div style="float: left;"><h2><strong>Projetos Finalizados</strong></h2></div>
        </div>

        <table id="category-finished-table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th width="5%">Nº de tarefas</th>
                    <th width="10%">HH</th>
                    <th width="5%">Ações</th>
                </tr>
            </thead>
            <tbody>
                <!-- Categorias = Projetos -->
                @foreach($categories_for_finished_table as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->tasksNumber }}</td>                        
                        <td>{{ $category->HH }}</td>
                        <td>           
                            <!-- Atributo id_category foi criado no componente a para passar o id do projeto ao modal -->
                            <a  href="#" 
                                id_category="{{ $category->id }}" 
                                class="showBtn fa fa-reply fa-2x reativar-projeto" 
                                title="Reativar"></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
@endsection

@section('inline_scripts')
<script>
    $("#category-table").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
        },
    });

    $("#category-finished-table").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
        },
    });

    // Modal de confirmação para finalizar o projeto
    $('.finalizar-projeto').click(function() {
        var category_id = $(this).attr("id_category");
        swal({
            title: 'Você deseja finalizar o projeto?',            
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Finalizar',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            $.ajax({
                headers: {
                'X-CSRF-Token': '{!! csrf_token() !!}'
                },
                type: 'POST',
                url: "{!! route('category.invertFinished') !!}",
                data: {id: category_id},
                enctype: 'multipart/form-data',
                success: function(){
                    swal('Projeto Finalizado','', 'success').then(
                        function () {
                            location.reload();
                        }
                    );                    
                },
                error: function(){
                    swal(
                    'Cancelado',
                    'O projeto não foi finalizado',
                    'error'
                    );
                }
            });
        }, function (dismiss) {
            if (dismiss === 'cancel') {
                swal(
                'Cancelado',
                'O projeto não foi finalizado',
                'info'
                );
            }
        });
    });

    // Modal de confirmação para reativar o projeto
    $('.reativar-projeto').click(function() {
        // Id do projeto pego pelo atributo id_category
        var category_id = $(this).attr("id_category");
        swal({
            title: 'Você deseja reativar o projeto?',            
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Reativar',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                },
                type: 'POST',
                url: "{!! route('category.invertFinished') !!}",
                data: {id: category_id},
                enctype: 'multipart/form-data',
                success: function(){
                    swal('Projeto Reativado','', 'success').then(                        
                        function () {
                            location.reload();
                        }
                    );                    
                },
                error: function(){
                    swal(
                    'Erro',
                    'O projeto não foi reativado.',
                    'error'
                    );
                }
            });
        }, function (dismiss) {
            if (dismiss === 'cancel') {
                swal(
                'Cancelado',
                'O projeto não foi reativado.',
                'info'
                );
            }
        });
    });
</script>
@endsection

