@extends('layouts.master')

@section('content')

{{--  Fix de bug no footer da box  --}}
<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
		<div class="box box-primary box-create">
			<div class="box-header with-border">
				@if(isset($category))
					<h3 class="box-title">Editar Projeto</h3>
				@else
					<h3 class="box-title">Criar Projeto</h3>
				@endif
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			@if(isset($category))
			{!! Form::open(['method' => 'PUT', 'route' => ['category.update', $category->id]]) !!}
			@else
			{!! Form::open(['method' => 'POST', 'route' => ['category.store']]) !!}
			@endif

            <div class="box-body">

                <div class="form-group">
                    <label for="name">Nome</label>
                    {!! Form::text('name', (isset($category) ? $category->name : null), ['class' => 'form-control']) !!}
                </div>

				<div class="form-group">
                    <label for="HH">Valor HH</label>
                    {!! Form::text('HH', (isset($category) ? $category->HH : null), ['class' => 'form-control', 'pattern' => '[0-9 ]*[,.]*[0-9]*',  'oninvalid' => "this.setCustomValidity('Please enter a number.')", 'oninput' => "setCustomValidity('')" ]) !!}
                </div>

            
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection