<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SAO - Sistema de Apontamentos Online</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/adminlte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/adminlte/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/adminlte/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="/adminlte/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="/adminlte/plugins/datepicker/datepicker3.css">
  <!-- <link rel="stylesheet" href="/assets/libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"> -->
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/adminlte/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- SweetAlert -->
  <link rel="stylesheet" href="/assets/libs/sweetalert2/dist/sweetalert2.css" media="all"/>
  <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/plugins/select2/select2.min.css">
  <link rel="stylesheet" href="/assets/libs/select2/dist/css/select2.min.css" />
  <link rel="stylesheet" href="/assets/libs/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="/assets/css/custom.css">

  <!-- Data Table -->
  <link rel="stylesheet" type="text/css" href="/assets/css/jquery.dataTables.min.css" />

  <link rel="shortcut icon" type="image/png" href="/favicon.png">

  <!-- Hint -->
  <link rel="stylesheet" href="/assets/libs/hint.css/hint.base.css">

  
  
  <!-- Styles -->
  @section('specific_styles')
  @show

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
  <!-- INICIO VIEW PONTO -->

  <style type="text/css">
    .fixed-nav-bar {
      position: fixed;
      top: 0;
      z-index: 1000;
      right: 0;
      height: 55px;
      background-color: #3c8dbc;
      width: 100%;
      padding: 10px 15px 5px 0px;
    }
    
    /* Classe necessária para arrumar o bug na exibição de comentários
    em apontamentos */
    [class*="hint--"] {
      display: table-cell !important;
    }

    .element-nav-bar {
      color: white;
      display: inline-block;
      vertical-align: super;
    }

    .size-select {
      width: 210px;
    }

    a.info{
      position:relative; /*this is the key*/
      z-index:24;
      text-decoration:none
    }

    a.info:hover{z-index:25;}

    a.info span{display: none;}

    a.info:hover span{ /*the span will display just on :hover state*/
      display: block;
      position: absolute;
      font-weight: bold;
      top: 35px;
      right: 0%;
      width: 130px;
      margin-left: -125px;
      border: 2px solid #ecf0f5;
      background-color: #3c8dbc;
      color: #ffffff;
      text-align: center;
      z-index: 9999999;
      text-decoration: none;
    }
    #div-esq {
     width:350px;
     float:left;
     position: relative;
     left: 6.5%;
     top: 4px;
    }
    #div-esq-name {
      width:150px;
      float:left;
      position: relative;
      left: 50px;
    }
    #div-esq-pay {
        width:30px;
        float:left;
    }
    @media only screen and (max-width: 1200px) {
      /* For mobile phones: */
      #div-esq {
        position: relative;
        left: 4.5%!important;
      }
    }
    @media only screen and (max-width: 1173px) {
      /* For mobile phones: */
      #div-esq {
        position: relative;
        left: 3.5%!important;
      }
    }
    @media only screen and (max-width: 1159px) {
      /* For mobile phones: */
      #div-esq {
        width:300px;
        position: relative;
        left: 7%!important;
      }
    }
    @media only screen and (max-width: 1148px) {
      /* For mobile phones: */
      #div-esq {
        width:280px;
        position: relative;
        left: 7%!important;
      }
    }
    @media only screen and (max-width: 1128px) {
      /* For mobile phones: */
      #div-esq {
        width:280px;
        position: relative;
        left: 4%!important;
      }
    }
    @media only screen and (max-width: 1090px) {
      /* For mobile phones: */
      #div-esq {
        width:200px;
        position: relative;
        left: 4%!important;
      }
    }
    @media only screen and (max-width: 1000px) {
      /* For mobile phones: */
      #div-esq {
        width:100px;
        position: relative;
        left: 4%!important;
      }
    }

}
  </style>

  <div class="fixed-nav-bar">
    {!! Form::model(null, ['method' => 'POST']) !!}
    <div style="font-size: large;" id="div-esq-name" class="element-nav-bar"> Tempo Restante:&nbsp;&nbsp;</div>
    
    @if(Auth::user()->company == NULL || Auth::user()->company->expired_at == NULL)

    <div class="progress" id="div-esq" >
  <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar"
  aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width:{!! request()->time !!}%"  >
  Acesso Vitalício
  </div>  
</div>

    @else

<div class="progress" id="div-esq" >
  <div class="progress-bar progress-bar-striped progress-bar-danger active" role="progressbar"
  aria-valuenow="0" aria-valuemin="0" aria-valuemax="30" style="width:{!! request()->time !!}%"  >
  {!! request()->days !!} dias restantes
  </div>  
</div>
  <span>
  <a href="/pagamentos" id="div-esq-pay " class="btn btn-info" role="button">PAGAMENTO</a>
  </span>

   @endif

    <div style="float: right;" id="nav-bar-ponto">
      <div style="font-size: large;" class="element-nav-bar"> Ponto Rápido:&nbsp;&nbsp;</div>
      <div class="element-nav-bar">
        {!! Form::select('category_id', ['' => ''], '', ['id' => 'category_id_bar', 'class' => 'form-control select2 size-select', 'placeholder' => 'Selecione um projeto...']) !!}
      </div>
      <div class="element-nav-bar">
        {!! Form::select('task_id', ['' => ''], '', ['id' => 'task_id_bar', 'class' => 'form-control select2 size-select', 'disabled' => 'disabled']) !!}
      </div>
      &nbsp;
      <div class="element-nav-bar" style="vertical-align: baseline;">
        <a class="info" href="#" style="color: white" id="criar-ponto-rapido">
          <i class="fa fa-play-circle fa-2x" aria-hidden="true"></i>
          <span>Iniciar ponto</span>
        </a>
      </div>
      &nbsp;
      <div class="element-nav-bar" style="vertical-align: baseline;">
        <a class="info" href="#" style="color: white" id="encerrar-ponto-rapido">
          <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
          <span>Encerrar ponto ativo</span>
        </a>
      </div>
    </div>
    {!! Form::close() !!}
  </div>

  <!-- FIM VIEW PONTO -->


  <div class="wrapper">

    {{-- @include('layouts.partials.header') --}}

    @include('layouts.partials.sidebar') 

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      @include('flash::message')

      <section class="content-header">
        @section('header_title')
        @show
      </section>
      @yield('content')
    </div>
    <!-- /.content-wrapper -->
    {{-- @include('layouts.partials.footer') --}}

    {{-- @include('layouts.partials.control-sidebar') --}}

    <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/adminlte/plugins/jQuery/jquery-ui.min.js"></script>
<!-- Loading the vanilla-masker lib -->
<script src="/assets/js/vanilla-masker.min.js"></script>
<!-- Mask Money -->
<script src="/assets/js/jquery.maskMoney.min.js" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="/assets/js/raphael-min.js"></script>
<script src="/adminlte/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/adminlte/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/adminlte/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="/assets/js/moment.min.js"></script>
<!-- <script src="/assets/libs/moment/locale/pt-br.js"></script> -->
<script src="/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/assets/libs/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- jQuery Mask -->
<!-- <script src="/assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- Select2 -->
<script src="/assets/libs/select2/dist/js/select2.min.js"></script>

<!-- Highcharts highcharts.js -->
<script src="/assets/libs/highcharts/highcharts.js"></script>
<script src="/assets/libs/highcharts/highcharts-3d.js"></script>
<script src="/assets/libs/offline-exporting.js"></script>

<!-- AdminLTE App -->
<script src="/adminlte/dist/js/app.min.js"></script>
{{-- <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="/adminlte/dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="/adminlte/dist/js/demo.js"></script> --}}
  <script src="/assets/libs/sweetalert2/dist/sweetalert2.js"></script>

  <!-- Data Table -->
  <script src="/assets/js/jquery.dataTables.min.js"></script>


  <!-- Old Sweet Alert Version -->
  <link href="/assets/css/chosen.min.css" rel="stylesheet"/>
  <script src="/assets/js/chosen.jquery.min.js"></script>

  @section('specific_scripts')
  @show


  <script>
    var flashNotification = function(message, type) {
    // Notification
    toastr.options = {
      "preventDuplicates": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-bottom-right",
      "timeOut": "2500",
      "extendedTimeOut": "500",
      "showMethod": "slideDown",
      "hideMethod": "fadeOut",
    }

    if(type === 'success')
      toastr.success(message, 'Sucesso');
    else
      toastr.error(message, 'Falha');
  }

  //Date picker
  $('.datepicker').datetimepicker({
    format: 'DD/MM/YYYY HH:mm'
  });

  // Requisição ajax para preencher o select de projetos
  $.ajax({
    url: '{!! route('apointments.partials.ajax_select_categories') !!}',
    method: 'GET',
    success: function(data) {
      $("select[id='category_id_bar']").html(''); 
      $("select[id='category_id_bar']").html(data.options);
      if(data.options == '') {
        $("select[id='category_id_bar']").attr("disabled", true);
      } else {
        $("select[id='category_id_bar']").attr("disabled", false);
      }
    }
  });
  

  $('select').select2();

  $("select[id='category_select']").change(function(){
    var category_id = $(this).val();
    var token = $("input[name='_token']").val();
    $.ajax({
      url: '{!! route('apointments.partials.ajax_select') !!}',
      method: 'POST',
      data: {category_id:category_id, _token:token},
      success: function(data) {
        $("select[id='task_select']").html('');
        $("select[id='task_select']").html(data.options); 
        if(data.options == '') {
          $("select[id='task_select']").attr("disabled", true);
        } else {
          $("select[id='task_select']").attr("disabled", false);
        }
      }
    });
  });

  $("select[id='category_id_bar']").change(function(){
    var category_id = $(this).val();
    var token = $("input[name='_token']").val();
    $.ajax({
      url: '{!! route('apointments.partials.ajax_select') !!}',
      method: 'POST',
      data: {category_id:category_id, _token:token},
      success: function(data) {
        $("select[id='task_id_bar']").html('');
        $("select[id='task_id_bar']").html(data.options); 
        if(data.options == '') {
          $("select[id='task_id_bar']").attr("disabled", true);
        } else {
          $("select[id='task_id_bar']").attr("disabled", false);
        }
      }
    });
  });

  // se preecher salario fixo habilitar projeto



  $('#criar-ponto-rapido').click(function() {
    var category_id = $('#category_id_bar').val();
    var task_id = $('#task_id_bar').val();
    if(category_id == '' || task_id == '') {
      swal('Por favor, preencha todos campos', 'Categoria; Tarefa', 'error')
    } else {
      swal({
        title: 'Confirmar criação de ponto?',
        text: "Você não poderá desfazer essa ação!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Criar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
       $.ajax({
        headers: {
          'X-CSRF-Token': '{!! csrf_token() !!}'
        },
        type: 'POST',
        url: '{!! route('apointment.computar_horas') !!}',
        data: {category_id:category_id, task_id:task_id},
        enctype: 'multipart/form-data',
        success: function(data){
          if(data.status == null) {
            swal('Já há um ponto ativo!', 'Encerre-o antes de iniciar outro', 'error');
          } else {
            swal('Ponto registrado: ', data.status, 'success').then(
              function () {
                location.reload();
              }
              );
          }
        },
        error: function(){
          swal(
            'Cancelado',
            'O apontamento não foi registrado',
            'error'
            )
        }
      });
     }, function (dismiss) {
      if (dismiss === 'cancel') {
        swal(
          'Cancelado',
          'O apontamento não foi registrado',
          'info'
          )
      }
    });
    }
  });

  $(document).on('click', '#encerrar-ponto-rapido', function() {

      swal.setDefaults({
        input: 'text',
        confirmButtonText: 'Next &rarr;',
        showCancelButton: false,
        animation: false,
        progressSteps: ['1', '2']
      })

      var steps = [
        {
          title: 'Deseja encerrar o apontamento?',
          text: "Deixe um comentário abaixo se desejar",
          input: 'textarea',
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Encerrar!',
          cancelButtonText: 'Cancelar',
          allowOutsideClick: false
        },
        {
          title: 'Qual o andamento da tarefa?',
          text: "Escreva a porcentagem de conclusão:",
          type: 'input',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Encerrar!',
          allowOutsideClick: false
        }
      ]

      swal.queue(steps).then(function (result) {
        swal.resetDefaults()

        $.ajax({
          headers: {
            'X-CSRF-Token': '{!! csrf_token() !!}'
          },
          type: 'POST',
          url: '{!! route('apointment.check_last') !!}',
          data: {info:result},
          enctype: 'multipart/form-data',
          success: function(data){
            if (data.success) {
							swal(
								'Apontamento encerrado!',
								'Horas trabalhadas: ' + data.status,
								'success'
							).then(function () {
                  location.reload();
                });
            }
            else if(data.gt_eight_hours) {
              swal({
                    title: 'Deseja realmente registrar o apontamento com mais de 8h?',
                    text: "O apontamento que você está registrando é de "+data.status,
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim!',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                  }).then(function(){
                    $.ajax({
                      headers: {
                        'X-CSRF-Token': '{!! csrf_token() !!}'
                      },
                      type: 'POST',
                      url: '{!! route('apointment.check_last') !!}',
                      data: {
                        info:result,
                        gt_eight_hours: true
                      },
                      enctype: 'multipart/form-data',
                      success: function (data) {
                        swal(
                          'Apontamento encerrado!',
                          'Horas trabalhadas: ' + data.status,
                          'success'
                        ).then(function () {
                          location.reload();
                        });
                      },
                      error: function () {
                        swal(
                          'Cancelado',
                          'O apontamento não foi encerrado',
                          'error'
                        )
                      }
                    });
                  });
						}
            else{
							swal(
								'Apontamento não encerrado',
								data.message,
								'error'
								);
						}
          },
          error: function(){
            swal(
              'Cancelado',
              'O apontamento não foi encerrado',
              'error'
            )
          }
        }); 
      }, function () {
        swal.resetDefaults()
      })

    // swal({
    //   title: 'Deseja encerrar apontamento atual?',
    //   text: "Deixe um comentário abaixo se desejar",
    //   input: 'textarea',
    //   type: 'question',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Encerrar!',
    //   cancelButtonText: 'Cancelar',
    //   allowOutsideClick: false
    // }).then(function (textarea) {
    //   $.ajax({
    //     headers: {
    //       'X-CSRF-Token': '{!! csrf_token() !!}'
    //     },
    //     type: 'POST',
    //     url: '{!! route('apointment.check_last') !!}',
    //     data: {comment:textarea},
    //     enctype: 'multipart/form-data',
    //     success: function(data){
    //       if(data.status == null) {
    //         swal(
    //           'Cancelado',
    //           'Não há apontamento aberto',
    //           'error'
    //           )
    //       } else {
    //         swal(
    //           'Apontamento encerrado!',
    //           'Horas trabalhadas: ' + data.status,
    //           'success'
    //           ).then(function () {
    //             location.reload();
    //           });
    //         }
            
    //       },
    //       error: function(){
    //         swal(
    //           'Cancelado',
    //           'O apontamento não foi encerrado',
    //           'error'
    //           )
    //       }
    //     }); 
    // });

  });

</script>

@section('inline_scripts')
@show

</body>
</html>
