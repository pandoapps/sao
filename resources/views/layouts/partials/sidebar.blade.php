<aside class="main-sidebar" style="padding-top: 55px;">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
    <li class="header sidebar-header">
        <p>Bem-vindo(a): {{Auth::user()->name}}</p>
      </li>
      <li class="header sidebar-header">
          <span>APONTAMENTOS</span>
      </li>
      <li class="">
        <a href="{{ route('apointment.ativos') }}">
          <i class="fa fa-spinner fa-fw"></i>
          <span>DASHBOARD</span>
        </a>
      </li>
      <li class="">
        <a href="{{ route('apointment.index') }}">
          <i class="fa fa-list fa-fw"></i>
          <span>Meus Apontamentos</span>
        </a>
      </li>
      @if(Auth::user()->can(['see-reports']))
        <li class="">
          <a href="#">
            <i class="fa fa-bar-chart fa-fw"></i>
            <span>Relatórios</span>
          </a>
          <ul class="treeview-menu">
              <!-- Categorias = Projetos -->
              <li><a href="{{ route('apointment.relatorio',['type'=>'categorias']) }}"><i class="fa fa-circle-o"></i> Projetos</a></li>
              <li><a href="{{ route('apointment.relatorio',['type'=>'colaboradores']) }}"><i class="fa fa-circle-o"></i> Colaboradores</a></li>
          </ul>
        </li>
      @endif
      <!-- <li class="">
        <a href="{{ route('apointment.hour_storage') }}">
          <i class="fa fa-clock-o fa-fw"></i>
          <span>Banco de Horas</span>
        </a>
      </li> -->


      <li class="header sidebar-header">
          <span>PROJETOS</span>
      </li>
      <li class="">
        <!-- Categorias = Projetos -->
        <a href="{{ route('category.index') }}">
          <i class="fa fa-folder"></i>
          <span>Projetos</span>
        </a>
      </li>
      <li class="">
        <a href="{{ route('task.index') }}">
          <i class="fa fa-tasks fa-fw"></i>
          <span>Tarefas</span>
        </a>
      </li>

      @if(Auth::user()->can(['manage-roles','manage-users']))
        <li class="header sidebar-header">
            <span>USUÁRIOS</span>
        </li>
        @if(Auth::user()->can(['manage-roles']))
          <li class="">
            <a href="{{ route('role.index') }}">
              <i class="fa fa-address-card"></i>
              <span>Perfis e Permissões</span>
            </a>
          </li>
        @endif
        @if(Auth::user()->can(['manage-users']))
          <li class="">
            <a href="{{ route('user.index') }}">
              <i class="fa fa-users fa-fw"></i>
              <span>Usuários</span>
            </a>
          </li>
        @endif
      @endif

      @if(Auth::user()->can(['manage-company']))
        <li class="header sidebar-header">
            <span>EMPRESAS</span>
        </li>

        <!-- Opção de relatórios financeiros -->
        <li class="">
        <a href="{{ route('company.index') }}">
          <i class="fa fa-users fa-fw"></i>
            <span>Empresas</span>
          </a>
        </li>                

        
      @endif
      
      @if(Auth::user()->can(['see-financial-reports']))
        <li class="header sidebar-header">
            <span>FINANCEIRO</span>
        </li>
        @if(Auth::user()->can(['see-financial-reports']))
        <li class="">
          <a href="{{ route('cost.index') }}">
            <i class="fa fa-usd"></i>
            <span>Gastos</span>
          </a>
        </li>

        <!-- Opção de relatórios financeiros -->
        <li class="">
          <a href="{{ route('cost.relatorio') }}">
            <i class="fa fa-bar-chart fa-fw"></i>
            <span>Relatórios</span>
          </a>
        </li>  
        <!--<li class="">
        <a href="{{ route('logout') }}">
          <i class="fa fa-sign-out fa-fw"></i>
          <span>Sair</span>
        </a>
        </li>-->              
        @endif
      @endif
      <li class="">
        <a href="{{ route('logout') }}">
          <i class="fa fa-sign-out fa-fw"></i>
          <span>Sair</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>