
<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="utf-8">
	<title>SAO</title>
	
	<meta name="description" content="--">

	<meta name="keywords" content="Premium HTML Template">

	<meta name="author" content="HTMLmate">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- css-include -->

	<!-- boorstrap -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<!-- themify-icon.css -->
	<link rel="stylesheet" type="text/css" href="assets/css/themify-icons.css">
	<!-- animate.css -->
	<link rel="stylesheet" type="text/css" href="assets/css/animate.css">
	<!-- owl-carousel -->
	<link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
	<!-- video.min.css -->
	<link rel="stylesheet" type="text/css" href="assets/css/video.min.css">
	<!-- menu style -->
	<link rel="stylesheet" type="text/css" href="assets/css/menu.css">
	<!-- style -->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<!-- responsive.css -->
	<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
	<!-- SweetAlert -->
	<link rel="stylesheet" href="/assets/libs/sweetalert2/dist/sweetalert2.css" media="all"/>
</head>

<body>
	<!-- Start of Header 
		============================================= -->
		<header>
			<div id="main-menu"  class="main-menu-container tbg navbar-fixed-top">
				<div  class="main-menu">

					<div class="container">
						<div class="row">
							<div class="navbar navbar-default" role="navigation">
								<div class="container-fluid">
									<!-- Brand and toggle get grouped for better mobile display -->
									<div class="navbar-header">
										<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
											<span class="sr-only">Toggle navigation</span>
											<i class="ti-menu"></i>
										</button><!-- /.navbar-toggle collapsed -->
										<a class="navbar-brand text-uppercase" href="#">SAO</a>
									</div><!-- /.navbar-header -->

									<!-- Collect the nav links, forms, and other content for toggling -->
									<nav class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
										<ul id="main-nav" class="nav navbar-nav">
											<li><a href={!! route('login-form') !!}>Login</a></li>
											<!-- <li><a href="#about">about</a></li>
											<li><a href="#extra-features">Features</a></li>
											<li><a href="#showcase">showcase</a></li>
											<li><a href="#pricing">pricing</a></li>
											<li><a href="#download-area">DOWNLOAD<span class="ti-import"></span></a></li>
											<li><a href="#contact">contact</a></li> -->
										</ul><!-- /#main-nav -->
									</nav><!-- /.navbar-collapse -->
								</div><!-- /.container-fluid -->
							</div><!-- /.navbar navbar-default -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</div><!-- /.full-main-menu -->
			</div><!-- #main-menu -->
			<!-- Main Menu end -->
		</header> <!-- .cd-auto-hide-header -->
	<!-- End of Header 
		============================================= -->




	<!-- Start of banner section 
		============================================= -->
		<section id="banner-area" class="banner-area-section home-2">
			<div class="container">
				<div class="row">
					<div class="banner-area-content">
						<div class="banner-area-head">							
							<h1 class="pb30">Acompanhe sua equipe</h1>
							<div  class="banner-text pb40">
								<span>Boa parte dos gastos de uma empresa de projetos são os salários.</span>
								<br>
								<span>Que tal acompanhar em tempo real?</span>
							</div>
							<div class="download-store ul-li">
								<ul class="store-list">
									<li><button class="btn"><a href="#benefits">Benefícios</a></button></li>
									<li><button class="btn"><a href="#functionalities">Funcionalidades</a></button></li>
								</ul>
							</div> 

							<div class="banner-mock-up home-1 zooming">
								<img src="assets/img/mock-up/charts-1.png" alt="img">
							</div>

						</div><!-- /banner-area-head -->
					</div><!-- /banner-area-content -->
				</div><!-- /row -->
			</div><!-- /.container -->
		</section>
	<!-- End of banner section  
		============================================= -->




	<!-- Início da seção de benefícios
		============================================= -->
		<section id="benefits" class="features-section">
			<div class="container">				
				<div class="row section-content mt50">
					<h2 class="header-section">Benefícios</h2>
					<div class="features-content">
						<div class="row mt50">
							<div class="col-md-4">
								<div class="features-text-icon text-center">
									<div class="features-icon">
										<i class="orange-gred ti-money"></i>
									</div>
									<!-- //icon -->
									<div class="features-text mt25">
										<div class="features-text-title pb10">
											<h3 class="deep-black">Econômico</h3>
										</div>
										<div class="features-text-dec">
											<span>Sem custos de implantação.</span>
										</div>
									</div>
									<!-- //text -->

								</div><!-- // features-text-icon -->
							</div>
							<!-- // col-sm-4 -->


							<div class="col-md-4">
								<div class="features-text-icon text-center">
									<div class="features-icon">
										<i class="orange-gred ti-timer"></i>
									</div>
									<!-- //icon -->
									<div class="features-text mt25">
										<div class="features-text-title pb10">
											<h3 class="deep-black">Intuitivo</h3>
										</div>
										<div class="features-text-dec">
											<span>Rápido e fácil de usar.</span>
										</div>
									</div>
									<!-- //text -->

								</div><!-- // features-text-icon -->
							</div>
							<!-- // col-sm-4 -->

							<div class="col-md-4">
								<div class="features-text-icon text-center">
									<div class="features-icon">
										<i class="orange-gred ti-face-smile"></i>
									</div>
									<!-- //icon -->
									<div class="features-text mt25">
										<div class="features-text-title pb10">
											<h3 class="deep-black">Produtivo</h3>
										</div>
										<div class="features-text-dec">
											<span>Aumente o engajamento da equipe.</span>
										</div>
									</div>
									<!-- //text -->

								</div><!-- // features-text-icon -->
							</div>
							<!-- // col-sm-4 -->

						</div><!-- /section-row -->
					</div><!-- /features-content -->
				</div><!-- /row -->
			</div><!-- /container -->
		</section>
	<!-- Fim da seção de benefícios  
		============================================= -->





	<!-- Start of About Section 
		============================================= -->
		<!-- <section id="about" class="about-section">
			<div class="container">
				<div class="row section-content">
					<div class="about-section-content">
						<div class="row">
							<div class="col-md-6">
								<div class="about-left-img zooming">
									<img src="assets/img/mock-up/about.png" alt="img">
								</div>
							</div>

							<div class="col-md-6">
								<div class="about-section-text">
									<div class="section-title text-left pb50">
										<span class="title-text pink text-uppercase">AVAILABLE NOW</span>
										<h1 class="title deep-black pb40">Best Landing Page Solution</h1>
										<span class="title-dec">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolorin.
										</span>
									</div>

									<div class="download-store ul-li">
										<ul class="store-list">
											<li><a href="https://play.google.com/store?hl=en" target="blank"><img src="assets/img/store/google.png"></a></li>
											<li><a href="https://itunes.apple.com/us/app/apple-store/id375380948?mt=8" target="blank"><img src="assets/img/store/apple.png"></a></li>
										</ul>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End of About Section  
		============================================= -->





	<!-- Início da seção de funcionalidades 
		============================================= -->
		<section id="functionalities" class="extra-features-section">
			<div class="container">
				<div class="row section-content mt5">
					<div class="section-title text-center pb40">
						<h2 class="header-section">Funcionalidades</h2>
					</div>
					<!-- //section-title -->
					<div class="extra-features-content">
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="extra-left">
									<div class="extra-left-content">
										<div class="extra-icon-text text-left">
											<div class="features-icon text-center">
												<i class="orange-gred ti-control-forward"></i>
											</div>
											<div class="features-text pt25">
												<div class="features-text-title text-right pb10">
													<h3 class="deep-black">Ágil</h3>
												</div>
												<div class="features-text-dec text-right">
													<span>Aponte em dois cliques.</span>
												</div>
											</div>
										</div>
									</div>
									<!-- // extra-left-content -->

									<div class="extra-left-content">
										<div class="extra-icon-text text-left">
											<div class="features-icon text-center">
												<i class="orange-gred ti-thumb-up"></i>
											</div>
											<div class="features-text pt25">
												<div class="features-text-title text-right pb10">
													<h3 class="deep-black">Descomplicado</h3>
												</div>
												<div class="features-text-dec text-right">
													<span>Crie projetos com facilidade.</span>
												</div>
											</div>
										</div>
									</div>
									<!-- // extra-left-content -->

									<div class="extra-left-content pt45">
										<div class="extra-icon-text text-left">
											<div class="features-icon text-center">
												<i class="orange-gred ti-eye"></i>
											</div>
											<div class="features-text pt25">
												<div class="features-text-title text-right pb10">
													<h3 class="deep-black">Específico</h3>
												</div>
												<div class="features-text-dec text-right">
													<span>Monitore tarefas de forma independente.</span>
												</div>
											</div>
										</div>
									</div>
									<!-- // extra-left-content -->
								</div><!-- /extra-left -->
							</div>
							<!-- /col-sm-3 -->

							<div class="col-sm-4">
								<div class="extra-pic text-center">
									<img src="assets/img/mock-up/charts-2.png" alt="img">
								</div>
							</div>
							<!-- /col-sm-6 -->

							<div class="col-md-4 col-sm-6">
								<div class="extra-right">
									<div class="extra-left-content">
										<div class="extra-icon-text text-right">
											<div class="features-icon text-center">
												<i class="orange-gred ti-bar-chart"></i>
											</div>
											<div class="features-text pt25">
												<div class="features-text-title text-left pb10">
													<h3 class="deep-black">Completo</h3>
												</div>
												<div class="features-text-dec text-left">
													<span>Relatórios em tempo real.</span>
												</div>
											</div>
										</div>
									</div>
									<!-- // extra-left-content -->

									<div class="extra-left-content">
										<div class="extra-icon-text text-right">
											<div class="features-icon text-center">
												<i class="orange-gred ti-control-shuffle"></i>
											</div>
											<div class="features-text pt25">
												<div class="features-text-title text-left pb10">
													<h3 class="deep-black">Expectativa vs Realidade</h3>
												</div>
												<div class="features-text-dec text-left">
													<span>Cruzamento do que foi previsto com o relizado.</span>
												</div>
											</div>
										</div>
									</div>
									<!-- // extra-left-content -->

									<div class="extra-left-content">
										<div class="extra-icon-text text-right">
											<div class="features-icon text-center">
												<i class="orange-gred ti-stats-up"></i>
											</div>
											<div class="features-text pt25">
												<div class="features-text-title text-left pb10">
													<h3 class="deep-black">Estratégico</h3>
												</div>
												<div class="features-text-dec text-left">
													<span>Meça e melhore a cada dia.</span>
												</div>
											</div>
										</div>
									</div>
									<!-- // extra-left-content -->
								</div><!-- /extra-left -->
							</div>
							<!-- /col-sm-3 -->
						</div><!-- /row -->
					</div><!-- /extra-features-content -->
				</div><!-- /row -->
			</div><!-- /container -->
		</section>
	<!-- Fim da seção de funcionalidades
		============================================= -->




	<!-- Start of how to install   
		============================================= -->
		<!-- <section id="extra-features" class="how-to-install">
			<div class="container">
				<div class="row section-content">
					<div class="how-to-install-content">
						<div class="title-install text-center pb60">
							<h2>How To Install Landy</h2>
						</div>
						
						<div class="install-play text-center">
							<a class="popup-with-zoom-anim" href="https://www.youtube.com/watch?v=Rlc1FnJiPaI"><span class="orange-gred ti-control-forward"></span></a>
						</div>
						
					</div>
				</div>
			</div>
		</section> -->
	<!-- End of how to install   
		============================================= -->




	<!-- Start of showcase section  
		============================================= -->
		<!-- <section id="showcase" class="showcase-section">
			<div class="container">
				<div class="row section-content">
					<div class="showcase-content">
						<div class="section-title text-center pb50">
							<span class="title-text pink text-uppercase">LOVE TO SEE</span>
							<h1 class="title deep-black pb40">Our Showcase</h1>
						</div>
						
						<div class="showcase-slider">
							<div class="showcase-img">
								<img src="assets/img/showcase/show-1.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-2.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-3.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-4.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-1.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-2.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-3.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-4.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-1.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-2.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-3.jpg" alt="img">
							</div>
							<div class="showcase-img">
								<img src="assets/img/showcase/show-4.jpg" alt="img">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End of showcase section     
		============================================= -->




	<!-- Começo da seção de depoimentos   
		============================================= -->
		<section id="testimonial" class="testimonial-section">
			<div class="container">
				<div class="row section-content"> 
					<div class="testimonial-content">
						<div class="col-md-4">
							<div class="testimonial-name-designation">
								<div class="quate-icon">
									<span class="orange-gred ti-quote-left"></span>
								</div>
								<!-- //icon -->
								<div class="testimonial-text">
									<span>Ao monitorar a equipe senti uma melhora imediata de performance.</span>
								</div>
								<!-- //text -->
								<div class="testimonial-name-des  pt25">
									<div class="testimonial-pic">
										<img src="assets/img/testimonial/testi-1.jpeg" alt="img">
									</div>
									<!-- //pic -->
									<div class="client-designation pt20">
										<span class="name deep-black">Thiago Ferreira</span>
										<span class="designation">Pandô APPs</span>
									</div>
									<!-- //client-designation  -->
								</div>
								<!-- //name-dec -->
							</div><!-- /testimonial-name-designation -->	
						</div>
						<!-- //col-sm-4 -->


						<div class="col-md-4">
							<div class="testimonial-name-designation">
								<div class="quate-icon">
									<span class="orange-gred ti-quote-left"></span>
								</div>
								<!-- //icon -->
								<div class="testimonial-text">
									<span>No início relutei na implantação do SAO. Hoje não consigo me imaginar sem ele.</span>
								</div>
								<!-- //text -->
								<div class="testimonial-name-des  pt25">
									<div class="testimonial-pic">
										<img src="assets/img/testimonial/testi-2.jpeg" alt="img">
									</div>
									<!-- //pic -->
									<div class="client-designation pt20">
										<span class="name deep-black">Yago</span>
										<span class="designation">MoonTech</span>
									</div>
									<!-- //client-designation  -->
								</div>
								<!-- //name-dec -->
							</div><!-- /testimonial-name-designation -->	
						</div>
						<!-- //col-sm-4 -->



						<div class="col-md-4">
							<div class="testimonial-name-designation">
								<div class="quate-icon">
									<span class="orange-gred ti-quote-left"></span>
								</div>
								<!-- //icon -->
								<div class="testimonial-text">
									<span>Recomendo aos incubados da INCIT que façam uso e melhorem seus resultados.</span>
								</div>
								<!-- //text -->
								<div class="testimonial-name-des  pt25">
									<div class="testimonial-pic">
										<img src="assets/img/testimonial/testi-3.jpeg" alt="img">
									</div>
									<!-- //pic -->
									<div class="client-designation pt20">
										<span class="name deep-black">Maurício</span>
										<span class="designation">INCIT</span>
									</div>
									<!-- //client-designation  -->
								</div>
								<!-- //name-dec -->
							</div><!-- /testimonial-name-designation -->	
						</div>
						<!-- //col-sm-4 -->

					</div>
				</div><!-- /row -->
			</div><!-- /container -->
		</section>
	<!-- Fim da seção de depoimentos
		============================================= -->




	<!-- Início da seção de preços   
		============================================= -->
		<section id="pricing" class="pricing-plan-section">
			<div class="container">
				<div class="row section-content">
					<div class="pricing-plan-content">
						<div class="section-title text-center pb50">
							<h1 class="title deep-black pb40">Preços</h1>
						</div>
						<!-- //section-title -->
						<div class="pricing-plan">
							<div class="col-md-4 no-padding">
								<div class="landy-pricing text-center ul-li">
									<div class="header-pricing">
										<div class="pricing-price">
											<h3 class="content-price pink pb10">R$ 0</h3>
										</div>
										<div class="content-title mt10">
											<div class="deep-black text-uppercase">FREE</div>
										</div>
									</div>
									<!-- //header-pricing -->
									<div class="pricing-plan-list  pt35 pb40">
										<ul class="landy-pricing-content-desc">
											<li> Baixe e use em seu ambiente.
											</li>											
										</ul>
									</div>
									<!-- // pricing-plan-list -->
									<div class="landy-content-button text-uppercase">
										<a href="#">Download</a>
									</div>
								</div>
							</div>
						</div><!--  /landy-pricing -->

						<div class="pricing-plan">
							<div class="col-md-4 no-padding">
								<div class="landy-pricing middle text-center ul-li">
									<!-- <div class="landing-icon text-left">
										<span class="orange-gred ti-bookmark-alt"></span>
									</div> -->
									<div class="header-pricing">
										<div class="pricing-price">
											<h3 class="content-price pink pb10">R$ 50</h3>
											<span class="content-duration text-uppercase pb10">por ano</span>
										</div>
										<div class="content-title mt10">
											<div class="deep-black text-uppercase">STANDARD</div>
										</div>
									</div>
									<!-- //header-pricing -->
									<div class="pricing-plan-list  pt35 pb40">
										<ul class="landy-pricing-content-desc">
											<li>Use como serviço e aproveite das nossas melhorias.
											</li>											
										</ul>
									</div>
									<!-- // pricing-plan-list -->
									<div class="landy-content-button text-uppercase">
										<a class="create_company" href="#">R$ 50,00</a>
									</div>
								</div>
							</div>
						</div><!--  /landy-pricing -->


						<div class="pricing-plan">
							<div class="col-md-4 no-padding">
								<div class="landy-pricing text-center ul-li">
									<div class="header-pricing">
										<div class="pricing-price">
											<h3 class="content-price pink pb10">R$ 1000</h3>
											<span class="content-duration text-uppercase pb10">por ano</span>
										</div>
										<div class="content-title mt10">
											<div class="deep-black text-uppercase">ULTIMATE</div>
										</div>
									</div>
									<!-- //header-pricing -->
									<div class="pricing-plan-list  pt35 pb40">
										<ul class="landy-pricing-content-desc">
											<li>Use como serviço e solicite melhoramentos.
											</li>
											<br>
											<br>
											<br>
											<br>
										</ul>
									</div>
									<!-- // pricing-plan-list -->
									<div class="landy-content-button text-uppercase">
										<a class="create_company" href="#">R$ 1000,00</a>
									</div>
								</div>
							</div>
						</div><!--  /landy-pricing -->
					</div>
				</div><!--  /row-->
			</div><!--  /container -->

			

			<!-- Modal de cadastro da empresa -->
			{!! Form::open(['method' => 'PUT', 'route' => ['landing.ajax_register'], 'class' => 'modal multi-step', 'id' => 'registerModal']) !!}
				<div class="modal-dialog">								
					<div class="modal-content">
						<div class="modal-header">
							<!-- Headers das páginas do modals -->
							<h4 class="modal-title step-1" data-step="1" style="display: block;"><b>Cadastro de empresa</b></h4>
							<h4 class="modal-title step-2" data-step="2" style="display: none;"><b>Cadastro de empresa</b></h4>
							<h4 class="modal-title step-3" data-step="3" style="display: none;"><b>Cadastro do administrador</b></h4>
							<!-- Barra de progresso -->
							<div class="m-progress">
								<div class="m-progress-bar-wrapper">
									<br>
									<div class="m-progress-bar">
									</div>
								</div>
								<div class="m-progress-complete" style="display: none;">
									Completo
								</div>
							</div>
						</div>

						<!-- Primeira página -->
						<div class="modal-body step step-1">
							<br>
							<p class="text-center"><b>Cadastre a sua empresa e comece a utilizar a plataforma.</b></p>
							<br>
						</div>

						<!-- Segunda página -->						
						<div class="modal-body step step-2">
								<!-- Remove o foco do primeiro input -->
								<input type="hidden" autofocus="true" />

								<div class="form-group">
									<label for="companyName">Nome da empresa*</label>
									{!! Form::text('companyName', null, 
										['class' => 'form-control',
										'data-validation' => 'length',
										'data-validation-length' => 'min1', 
										'data-validation-error-msg' => 'É necessário um nome.' 
										]) !!}									
								</div>

								<div class="form-group">
									<label for="companyEmail">Email da empresa*</label>
									{!! Form::email('companyEmail', null, 
										['class' => 'form-control',
										'placeholder' => 'mail@website.com',
										'data-validation' => 'email', 
										'data-validation-error-msg' => 'É necessário um e-mail válido.' 
										]) !!}
								</div>

								<div class="form-group">
									<label for="companyAddress">Endereço da empresa</label>
									{!! Form::text('companyAddress', null, 
										['class' => 'form-control']) !!}
								</div>

								<div class="form-group">
									<label for="companyPhone">Telefone da empresa</label>
									{!! Form::text('companyPhone', null, 
										['class' => 'form-control',
										'placeholder' => '(DDD) XXXX-XXXX',
										'attrname'=> 'telephone1'
										]) !!}
								</div>
							
						</div>

						<div class="modal-body step step-3">
							<!-- Remove o foco do primeiro input -->
							<input type="hidden" autofocus="true" />

							<p>Agora cadastre o administrador da empresa:</p>
							<div class="form-group">
								<label for="adminName">Nome*</label>
								{!! Form::text('adminName', null, 
									['class' => 'form-control',
									'data-validation' => 'length',
									'data-validation-length' => 'min1', 
									'data-validation-error-msg' => 'É necessário um nome.' 
									]) !!}									
							</div>

							<div class="form-group">
								<label for="adminEmail">Email*</label>
								{!! Form::email('adminEmail', null, 
									['class' => 'form-control',
									'placeholder' => 'mail@website.com',
									'data-validation' => 'email', 
									'data-validation-error-msg' => 'É necessário um e-mail válido.' 
									]) !!}
							</div>

							<div class="form-group">
								<label for="adminPassword_confirmation">Senha*</label>
								{!! Form::password('adminPassword_confirmation',
									['class' => 'form-control',
									'data-validation' => 'strength',
									'strength' => '1',
									'data-validation-error-msg' => 'Senha muito fraca.' 
									]) !!}									
							</div>

							<div class="form-group">
								<label for="adminPassword">Confirmar Senha*</label>
								{!! Form::password('adminPassword', 
									['class' => 'form-control',
									'data-validation' => 'confirmation',
									'data-validation-error-msg' => 'A senha deve ser a mesma.' 
									]) !!}
							</div>

							<div class="form-group">
								<label for="adminPhone">Telefone*</label>
								{!! Form::text('adminPhone', null, 
									['class' => 'form-control',
									'placeholder' => '(DDD) XXXX-XXXX',
									'data-validation' => 'brphone',
									'data-validation-error-msg' => 'Digite um telefone válido.',  
									'attrname'=> 'telephone2'
									]) !!}
							</div>

							<div class="form-group">
								<label for="adminRole">Cargo</label>
								{!! Form::text('adminRole', null, 
									['class' => 'form-control']) !!}
							</div>

							<div class="form-group">
								<label for="adminCH">Carga Horária*</label>
								{!! Form::text('adminCH', null, 
									['class' => 'form-control',
									'data-validation' => 'number', 
									'data-validation-allowing' => 'range[1;1000],float', 
									'data-validation-decimal-separator' => ',',
									'data-validation-error-msg' => 'É necessário um número de horas entre 1 e 1000'
									]) !!}
							</div>
							
							<div class="form-group">
								<label for="adminCH">Ganho Por Hora*</label>
								{!! Form::text('adminHH', '0,00', 
									['class' => 'form-control']) !!}
							</div>

							<div class="form-group">
								<label for="adminCH">Salário Fixo*</label>
								{!! Form::text('adminFixedPay', '0,00', 
									['class' => 'form-control']) !!}
							</div>							
						</div>
						<div class="modal-footer">
							<div class="modal-button-close text-center">	
								<!-- <button type="button" class="modal-button" data-dismiss="modal">Close</button> -->
								<a class="form_company_close" href="#">Fechar</a>								
							</div>
							<div class="modal-button text-center">
								<a class="form_company step step-1" data-step="1" href="#" onclick="sendEvent('#registerModal', 2)">Prosseguir</a>
								<!-- Acrescentar no elemento abaixo quando o formulário estiver ok -> onclick="sendEvent('#registerModal', 3)"
								e retirar a classe isDisabled -->
								<a id="next2" class="form_company isDisabled step step-2" href="#" data-step="2" >Prosseguir</a>
								<!-- Acrescentar no elemento abaixo quando o formulário estiver ok -> onclick="register()"
								e retirar a classe isDisabled -->																
								<a id="submit" class="form_company isDisabled step step-3" href="#">Enviar</a>
							</div>
						</div>
					</div>				
				</div>
			{!! Form::close() !!}
		</section>
	<!-- Fim da seção de preços       
		============================================= -->




	<!-- Start of download        
		============================================= -->
		<!-- <section id="download-area" class="download-section">
			<div class="container">
				<div class="row section-content">
					<div class="download-area-content  text-center">
						<div class="download-number  pb20">
							<h2>7895</h2>
						</div>
						<div class="download-area-text  pb20">
							<span>Join our thoursands of satisfied family and get your copy to use!</span>
						</div>
						<div class="download-now pb40">
							<h3>Download Your App Now</h3>
						</div>
						<div class="download-store ul-li">
							<ul class="store-list">
								<li><a href="https://play.google.com/store?hl=en" target="blank"><img src="assets/img/store/google.png"></a></li>
								<li><a href="https://itunes.apple.com/us/app/apple-store/id375380948?mt=8" target="blank"><img src="assets/img/store/apple.png"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End of  download        
		============================================= -->




	<!-- Início da seção de contato       
		============================================= -->
		<section id="contact" class="contact-section">
			<div class="container">
				<div class="row section-content">
					<div class="section-title text-center pb50">
						<h1 class="title deep-black pb40">Entre em contato</h1>
					</div>
					<!-- //section-title -->

					<div class="comment-form">
						<form id="contact_form" action="#" method="POST" enctype="multipart/form-data">
							<div class="contact-info">
								<input class="name  mr30" name="name" type="text" placeholder="Nome*">
							</div>
							<div class="contact-info">
								<input class="email" name="email" type="text" placeholder="Email*">
							</div>
							<div class="contact-info-1">
								<input class="name" name="name" type="text" placeholder="Assunto">
							</div>
							<div class="contact-info">
								<textarea id="message" name="message" placeholder="Mensagem..." rows="7" cols="30"></textarea>
							</div>
							<div class="submit-btn text-center mt20">
								<button type="submit" value="Submit">Enviar</button>
							</div> 
						</form>
					</div>

				</div><!--  //row-->
			</div><!--  //container -->
		</section>
	<!-- Fim da seção de contato      
		============================================= -->




	<!-- Início da seção de footer    
		============================================= -->
		<footer>
			<section id="footer-area" class="footer-area-section">
				<div class="container">
					<div class="row section-content">
						<div class="footer-area-content">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-3">
									<div class="footer-content">
										<span class="right deep-black">2018. Desenvolvido colaborativamente pela comunidade SAO.</span>
										<!-- <div class="footer-address mt20">
											<span>457 Shantibag, Green Road 
												Philadelphia, PH USA 17512
												+1 437 800 2078
											</span>
										</div>
										<div class="footer-social ul-li mt20">
											<ul class="footer-social-list">
												<li><a href="#"><span class="ti-facebook"></span></a></li>
												<li><a href="#"><span class="ti-twitter-alt"></span></a></li>
												<li><a href="#"><span class="ti-google"></span></a></li>
												<li><a href="#"><span class="ti-vimeo-alt"></span></a></li>
											</ul>
										</div> -->
									</div>
								</div>
								<!-- //col-sm-4 -->

								<!-- <div class="col-md-3 col-sm-6">
									<div class="footer-service-list">
										<div class="footer-widget pb20">
											<h2 class="widgettile deep-black">Information</h2>
										</div>
										<div class="service-list ul-li ul-li-block">
											<ul class="service-list-item">
												<li><a href="#">Terms & Condision</a></li>
												<li><a href="#">About Us</a></li>
												<li><a href="#">Privacy Policy</a></li>
												<li><a href="#">Download</a></li>
											</ul>
										</div>
									</div>
								</div> -->


								<div class="right-align">
									<div class="footer-service-list">
										<div class="footer-widget pb20">
											<h2 class="widgettile deep-black">Apoio</h2>
										</div>
										<div class="service-list ul-li ul-li-block pl40">
											<ul class="service-list-item">
												<li><a href="https://www.pandoapps.com.br/" target="_blank"><img src="assets/img/logo/logo-pando.png" alt="img" height="100" width="100"></a></li>
												<li><a href="https://www.inovai.org.br/" target="_blank"><img src="assets/img/logo/logo-inovai.jpg" alt="img" height="100" width="100"></a></li>
											</ul>
										</div>
									</div>
								</div>
								<!-- //col-sm-4 -->

								<!-- <div class="col-md-3 col-sm-6">
									<div class="footer-widget pb20">
										<h2 class="widgettile deep-black">Subscribe</h2>
									</div>
									<span>Don’t miss out our every updates and news!</span>
									<div class="newsletter">
										<form action="#" method="get">
											<div class="newsletter-email">
												<input type="email" class="" >
												<button type="submit" value="Submit"><span class="orange-gred ti-arrow-right"></span></button>
											</div>
										</form>
									</div>
								</div> -->

							</div>
						</div><!--  //row -->
					</div><!--  //footer-area-content -->
				</div><!--  //container -->
			</section>
		</footer>
	<!-- Fim da seção de footer         
		============================================= -->


		<!--  Js Library -->
		<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
		<!-- Include  for bootstrap -->
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<!-- Include Owl-carousel -->
		<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
		<!-- Include OnePagenNav -->
		<!-- <script type="text/javascript" src="assets/js/OnePagenNav.js"></script> -->
		<!-- Include jquery.magnific-popup.min.js-->
		<script type="text/javascript" src="assets/js/jquery.magnific-popup.min.js"></script>
		<!-- Include script.js-->
		<script type="text/javascript" src="assets/js/script.js"></script>
		<!-- Multi-Step Modal -->
		<script src="/assets/js/multi-step-modal.js"></script>
		<!-- JQuey form Validator -->
		<script src="/assets/js/form-validator/jquery.form-validator.min.js"></script>
		<!-- Loading the vanilla-masker lib -->
		<script src="/assets/js/vanilla-masker.min.js"></script>
		<!-- Mask Money -->
		<script src="/assets/js/jquery.maskMoney.min.js" type="text/javascript"></script>
		<!-- Sweet Alert2 -->
		<script src="/assets/libs/sweetalert2/dist/sweetalert2.js"></script>

		<script>
			$(document).ready(function(){
				// Ao clicar nos botões de preço, um modal para o 
				// cadastro de empresa é aberto
				$('.create_company').click(function(e){
					$("#registerModal").modal();					
					
					// Previne o recarregamento da página
					e.preventDefault();
				});
				
				// Fechar o modal de cadastro da empresa
				$('.form_company_close').click(function(e){
					$("#registerModal").modal("hide");					
										
					// Previne o recarregamento da página
					e.preventDefault();
				});

				// Previne o recarregamento da página quando 
				// a página do modal é alterada
				$('.form_company').click(function(e){
					e.preventDefault();
				});

				// Faz a mudança entre as páginas do modal
				sendEvent = function(sel, step) {
					$(sel).trigger('next.m.' + step);
				}	

				// Início máscara de telefone
				function inputHandler(masks, max, event) {
					var c = event.target;
					var v = c.value.replace(/\D/g, '');
					var m = c.value.length > max ? 1 : 0;
					VMasker(c).unMask();
					VMasker(c).maskPattern(masks[m]);
					c.value = VMasker.toPattern(v, masks[m]);
				}

				var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
				var tel1 = document.querySelector('input[attrname=telephone1]');
				var tel2 = document.querySelector('input[attrname=telephone2]');
				VMasker(tel1).maskPattern(telMask[0]);
				VMasker(tel1).maskPattern(telMask[1]);
				VMasker(tel2).maskPattern(telMask[0]);
				VMasker(tel2).maskPattern(telMask[1]);
				tel1.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
				tel2.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);				
				// Fim máscara de telefone

				// Máscara de Carga Horária
				$(function() {
					$("input[name=adminCH]").maskMoney({thousands:'', decimal:',', affixesStay: false, precision: 1});
				})

				// Máscara de Ganho por hora
				$(function() {
					$("input[name=adminHH]").maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false, precision: 2, allowZero: true});
				})

				// Máscara de Salário Fixo
				$(function() {
					$("input[name=adminFixedPay]").maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false, precision: 2, allowZero: true});
				})

				
			});		
			
			// Requisição AJAX para registro da empresa e do administrador
			function register() {				
				// Entradas do formulário de registro
				var values = {};
				$.each($('form[id="registerModal"]').serializeArray(), function() {
					values[this.name] = this.value;
				});
				
				// Requisição ajax
				var request = $.ajax({
					headers: {
					'X-CSRF-Token': '{!! csrf_token() !!}'
					},
					type: "POST",
					url: "{{ route('landing.ajax_register') }}",					
					data: {values : values},
					enctype: 'multipart/form-data',
				});

				// Esconde o modal de cadastro 
				//$("#registerModal").modal("hide");

				// Sucesso da requisição ajax
				request.done(function(msg){
					swal({
						title: 'Cadatro Realizado',            
						type: 'success',	
						allowOutsideClick: false,					
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Login',
					}).then(function () {
						// Redireciona para o login
						location.replace("{{ route('login') }}");
					}); 
				});

				// Falha da requisição ajax
				request.fail(function(msg){
					swal({
						title: 'Erro', 
						text: 'Algum erro ocorreu no cadastro. Tente daqui a pouco.',           
						type: 'error',	
						allowOutsideClick: false,				
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Recarregar',
					}).then(function (){
						// Redireciona para a seção de pricing da landing page					
                        location.reload("{{ route('home') }}#pricing");
					});
				});
			}
		</script>

		<!-- Validação do Form de cadastro de empresa e usuário administrador -->
		<script>
			// Flags para auxiliarem na passagem da página 2 para
			// a página 3
			var companyNameIsValid = false;
			var companyEmailIsValid = false;

			// Flags para auxiliarem no submit do form
			var adminNameIsValid = false;
			var adminEmailIsValid = false;
			var adminPassIsValid = false;
			var adminPassConfirmIsValid = false;
			var adminPhoneIsValid = false;
			var adminCHIsValid = false;

			$.validate({
				lang : 	'pt',
				form : 	'#registerModal',
				modules : 'brazil, security',
				scrollToTopOnError: false,
				onElementValidate : function(valid, $el, $form, errorMess) {																		
					// Nome do elemento 
					var name = $el.attr('name');
					
					// Se o nome for válido a flag correspondente é setada
					if( name == 'companyName' ) {
						if(valid) {
							companyNameIsValid = true;						
						} else {
							companyNameIsValid = false;
						}
					}

					// Se o email for válido a flag correspondente é setada
					if( name == 'companyEmail' ) {
						if(valid) {
							companyEmailIsValid = true;
						} else {
							companyEmailIsValid = false;
						}						
					} 

					// Se o formulário da página de cadastro de empresa (página 2) é válido
					// permite o avanço para a página 3
					if(companyNameIsValid && companyEmailIsValid) {
						$('#next2').removeClass("isDisabled");
						$('#next2').attr("onclick","sendEvent('#registerModal', 3)");
					} else {
						$('#next2').addClass("isDisabled");
						$('#next2').removeAttr("onclick","sendEvent('#registerModal', 3)");
					}

					// Se o nome for válido a flag correspondente é setada
					if( name == 'adminName' ) {
						if(valid) {
							adminNameIsValid = true;						
						} else {
							adminNameIsValid = false;
						}
					}

					// Se o email for válido a flag correspondente é setada
					if( name == 'adminEmail' ) {
						if(valid) {
							adminEmailIsValid = true;
						} else {
							adminEmailIsValid = false;
						}						
					} 

					// Se a senha for válida a flag correspondente é setada
					if( name == 'adminEmail' ) {
						if(valid) {
							adminPassIsValid = true;
						} else {
							adminPassIsValid = false;
						}						
					} 

					// Se a confirmação de senha for válida a flag correspondente é setada
					if( name == 'adminPassword' ) {
						if(valid) {
							adminPassConfirmIsValid = true;
						} else {
							adminPassConfirmIsValid = false;
						}					
					} 

					// Se o telefone for válido a flag correspondente é setada
					if( name == 'adminPassword_confirmation' ) {
						if(valid) {
							adminPhoneIsValid = true;
						} else {
							adminPhoneIsValid = false;
						}						
					} 

					// Se a carga horária for válida a flag correspondente é setada
					if( name == 'adminCH' ) {
						if(valid) {
							adminCHIsValid = true;
						} else {
							adminCHIsValid = false;
						}						
					} 

					// Se o formulário da página de cadastro de administrador (página 3) é válido
					// permite o envio do form
					if( adminNameIsValid &&
						adminEmailIsValid &&
						adminPassIsValid &&
						adminPassConfirmIsValid &&
						adminPhoneIsValid &&
						adminCHIsValid ) 
					{
						$('#submit').removeClass("isDisabled");
						$('#submit').attr("onclick","register();");					
					} else {
						$('#submit').addClass("isDisabled");
						$('#submit').removeAttr("onclick","register();");
					}
				},
				onModulesLoaded : function() {
					var optionalConfig = {
						fontSize: '8pt',
						padding: '10px',
						bad : 'Muito Ruim',
						weak : 'Fraca',
						good : 'Boa',
						strong : 'Forte'
					};

					$('input[name="adminPassword_confirmation"]').displayPasswordStrength(optionalConfig);
				}
			});	

			$("#next2,#submit").on('click', function() {
				validaor = $('#registerModal').isValid();
			});
  		</script>
</body>
</html>