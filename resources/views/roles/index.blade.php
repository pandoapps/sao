@extends('layouts.master')

@section('content')
<div class="container-fluid">

    <div style="display: block; height: 90px">
		<div style="float: left;"><h2>Perfis</h2></div>
		<div style="float: right;">
			<h2>
				<a class="fa fa-plus-square" role="button" href="{{ route('role.create') }}"></a>
			</h2>
		</div>
    </div>
    
    <table id="roles-table" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Nome</th>
                <th>Nome para Exibição</th>
                <th>Descrição</th>
                <th width="5%">Ações</th>
			</tr>
        </thead>
        <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{{$role->name}}</td>
                    <td>{{$role->display_name}}</td>
                    <td>{{$role->description}}</td>
                    <td>
                        <a href="{{route('role.edit', ['id' => $role->id])}}" class="showBtn fa fa-pencil fa-2x" title="Editar"></a>
                        <a href="{{ route('role.destroy', ['id' => $role->id]) }}" class="showBtn fa fa-trash-o fa-2x"></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
	</table>

</div>
@endsection

@section('inline_scripts')
<script>
    $("#roles-table").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
        },
    });
</script>
@endsection

