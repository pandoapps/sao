@extends('layouts.master')

@section('content')

{{--  Fix de bug no footer da box  --}}
<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
		<div class="box box-primary box-create">
			<div class="box-header with-border">
				@if(isset($role))
					<h3 class="box-title">Editar Perfil</h3>
				@else
					<h3 class="box-title">Criar Perfil</h3>
				@endif
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			@if(isset($role))
			{!! Form::open(['method' => 'PUT', 'route' => ['role.update', $role->id]]) !!}
			@else
			{!! Form::open(['method' => 'POST', 'route' => ['role.store']]) !!}
			@endif

            <div class="box-body">

                <div class="form-group">
                    <label for="name">Nome</label>
                    {!! Form::text('name', (isset($role) ? $role->name : null), ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label>Nome para Exibição</label>
                    {!! Form::text('display_name', (isset($role) ? $role->display_name : null), ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
					<label>Descrição</label>
                    {!! Form::text('description', (isset($role) ? $role->description : null), ['class' => 'form-control']) !!}
                </div>

				<!-- Company Id Field -->
				@if(Auth::user()->company_id == null)
					<div class="form-group">
						<label>Empresa</label>
						{!! Form::select('company_id', ['' => 'Selecionar'] + $companies, null, ['class' => 'form-control']) !!}
					</div>
				@else
					{!! Form::hidden('company_id', Auth::user()->company->id) !!}
				@endif

				<!-- Permissions Field -->
				<div class="form-group">
					<label>Permissões</label>
					<br/>
					@foreach($permissions as $permission)
						<div class="form-group col-md-12 no-padding no-margin">
							@if(isset($role))
								<label class="checkbox-button-label">{!! Form::checkbox($permission->display_name, $permission->id, $role->permissions->contains($permission->id), ['name' => 'permissions[]', 'class' => 'field']) !!} {{$permission->display_name}}</label>
							@else
								<label class="checkbox-button-label">{!! Form::checkbox($permission->display_name, $permission->id, null, ['name' => 'permissions[]', 'class' => 'field']) !!} {{$permission->display_name}}</label>
							@endif
							@if(isset($permission->description))
								<i class="launch-tooltip fa fa-question-circle" data-toggle="tooltip" data-placement="top" style="cursor:help" title="{{ $permission->description }}"></i>
							@endif
						</div>
					@endforeach
				</div>


            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection