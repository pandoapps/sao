<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    use EntrustUserTrait { restore as private entrustUserRestore; }
    use SoftDeletes { restore as private softDeletesRestore; }

    public function restore()
    {
        $this->entrustUserRestore();
        $this->softDeletesRestore();
    }

    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    public static $rules = [
        'name' => 'required', 
        'email' => 'required|string|email|max:255|unique:users,email,id',
        'password' => 'required', 
        'company_id' => 'required', 
        'CH' => 'required', 
        'HH' => 'required', 
        'fixed_pay' => 'required',
        'phone' => 'required', 
        'role_id' => 'required',             
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'company_id', 'CH', 'HH', 'fixed_pay', 'phone', 'role_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function company() {
        return $this->belongsTo(Company::class, 'company_id');
    }

    // public function roles() {
    //     return $this->hasMany(Role::class);
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    /**
     * Always show HH with 2 decimal places and ',' as decimal separator
     * 
     */
    public function getHHAttribute($value)
    {
        // number_format(number,decimals,decimalpoint,thousandseparator) 
        return number_format($value*-1, 2, ',', '.');
    }

    /**
     * Always save HH with '.' as decimal separator and removes all spaces
     * 
     */
    public function setHHAttribute($value)
    {
        //$value = str_replace(' ', '', $value);
        $value = str_replace('.', '', $value);        
        $value = str_replace(',', '.', $value);

        return $this->attributes['HH'] = $value*-1;
    }

    /**
     * Always show fixed_pay with 2 decimal places and ',' as decimal separator
     * 
     */
    public function getFixedPayAttribute($value)
    {
        return number_format($value*-1, 2, ',', '.');
    }

    /**
     * Always save fixed_pay with '.' as decimal separator and negative
     * 
     */
    public function setFixedPayAttribute($value)
    {
        $value = str_replace('.', '', $value);        
        $value = str_replace(',', '.', $value);

        return $this->attributes['fixed_pay'] = $value*-1;
    }
}
