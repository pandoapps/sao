<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Auth;

use App\Category;
use App\Task;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if (config('app.force_https')) {
            $url->forceSchema('https');
        }

        try {
            // O nome é categories2 para não sobrepor outra variável que tem o mesmo nome
            view()->share('categories2', Category::where('finished_at', '=', null)->orderBy('name')->lists('name', 'id')->all());
            view()->share('tasks', Task::orderBy('name')->lists('name', 'id')->all());
        } catch(\Exception $e) {}
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
