<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Role;
use App\User;
use App\Company;
use App\Task;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->company_id === NULL){
            $users = User::all();
        } else {
            if (Auth::user()->roles()->first()->name=="admin") {
                $users = User::with("roles")->where('company_id', \Auth::user()->company_id)->get();
            } else {
                $users = User::where('company_id', \Auth::user()->company_id)
                    ->whereHas("roles", function($query){
                        $query->where("roles.name", "!=", "admin");
                    })->get();
            }
        }

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::orderBy('name','asc')->get()->pluck('name', 'id')->all();      
        $task = Task:: orderBy('name','asc')->get()->pluck('name', 'id')->all();
        if (Auth::user()->roles()->first()->name=="admin") {
            $roles = Role::orderBy('display_name','asc')->get()->pluck('display_name', 'id')->all();
        } else {
            $roles = Role::where('name', '!=' ,'admin')->orderBy('display_name','asc')->get()->pluck('display_name', 'id')->all();
        }

        return view('users.form', compact('roles', 'companies','task'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        
        $validation = Validator::make($input, User::$rules);
        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $user = new User;
        $user->CH = $request->CH;        
        $user->HH = $request->HH;
        $user->fixed_pay = $request->fixed_pay;
        $user->company_id = $request['company_id'];
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->category_sub = $request->category_id;
        $user->task_sub = $request->task_id;
        $user->save();
        $user->attachRole($request['role_id']);
       
        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $companies = Company::orderBy('name','asc')->get()->pluck('name', 'id')->all();
        if (Auth::user()->roles()->first()->name=="admin") {
            $roles = Role::orderBy('display_name','asc')->get()->pluck('display_name', 'id')->all();
        } else {
            $roles = Role::where('name', '!=' ,'admin')->orderBy('display_name','asc')->get()->pluck('display_name', 'id')->all();
        }

        return view('users.form', compact('user', 'roles', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        //dar um if isset($request['company_id'])
        if(isset($request['company_id'])){
            $user->company_id = $request['company_id'];
        }
        
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->role = $request->role;
        $user->email = $request->email;
        
        if($request->password != null)
        $user->password = bcrypt($request->password);
        
        $user->CH = $request->CH;
        $user->HH = $request->HH;
        // Salário Fixo do usuário
        $user->fixed_pay = $request->fixed_pay; 
        $user->save();
        if(isset($request['role_id'])){
            $user->roles()->sync([$request['role_id']]);
        }
        
        return redirect(route('user.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        if(Auth::user()->id===(int)$id){
            return response(['message' => 'Unauthorized'], 401);
        }
        $user = User::find($id);
        $user->delete();
        return redirect(route('user.index'));
    }
}
