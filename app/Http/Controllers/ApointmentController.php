<?php

namespace App\Http\Controllers;

use Flash;

use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;
use App\Http\Requests;
use App\Http\Requests\ApointmentRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Apointment;
use App\Category;
use App\Task;
use App\User;
use Carbon\Carbon;

class ApointmentController extends Controller
{
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('apointments.index');
        }

        public function ativos(){
            return view('apointments.ativos');
        }

        public function ajax_apointments(Request $request){
            $apointments = Apointment::select([
                'apointments.*',
                'categories.name as category_name',
                'tasks.name as task_name', 
                DB::raw('DATE_FORMAT(start, "%d/%m/%Y às %H:%i") as start_f'),
                DB::raw('DATE_FORMAT(end, "%d/%m/%Y às %H:%i") as end_f'),
                DB::raw('DATE_FORMAT(apointments.created_at, "%d/%m/%Y às %H:%i") as created_f'),
                DB::raw('DATE_FORMAT(apointments.updated_at, "%d/%m/%Y às %H:%i") as updated_f')
            ])
            ->join('tasks', 'task_id', '=', 'tasks.id')
            ->join('categories', 'tasks.category_id', '=', 'categories.id')
            ->where('user_id', Auth::user()->id);

            return Datatables::of($apointments)->make(true);
        }

        public function ajax_ativos(){
            $apointments = Apointment::select([
                'apointments.*',
                'users.name as user_name',
                'categories.name as category_name',
                'tasks.name as task_name', 
                DB::raw('DATE_FORMAT(start, "%d/%m/%Y às %H:%i") as start_f'),
                DB::raw('DATE_FORMAT(end, "%d/%m/%Y às %H:%i") as end_f'),
                DB::raw('DATE_FORMAT(apointments.created_at, "%d/%m/%Y às %H:%i") as created_f'),
                DB::raw('DATE_FORMAT(apointments.updated_at, "%d/%m/%Y às %H:%i") as updated_f')
            ])
            ->join('tasks', 'task_id', '=', 'tasks.id')
            ->join('categories', 'tasks.category_id', '=', 'categories.id')
            ->join('users', 'user_id', '=', 'users.id')
            ->where('end', null);

            if(Auth::user()->company_id !== NULL){
                $apointments = $apointments->where('apointments.company_id', Auth::user()->company_id);
            }

            return Datatables::of($apointments)->make(true);
        }

        /**
         * Show the application selectCategoriesAjax.
         *
         * @return \Illuminate\Http\Response
         */
        public function ajax_select_categories()
        {

            $categories = Category::where('finished_at', '=', null)->where('company_id', Auth::user()->company_id)->orderBy('name')->lists('name', 'id');

            $data = view('apointments.partials.ajax_select_categories',compact('categories'))->render();
            return response()->json(['options'=>$data]);            
        }

        /**
         * Show the application selectAjax.
         *
         * @return \Illuminate\Http\Response
         */
        public function ajax_select(Request $request)
        {
            if($request->ajax()){
                //$tasks = Task::where('category_id',$request->category_id)->pluck("id","name")->all();
                
                $tasks = DB::table('tasks')->where('category_id',$request->category_id)->orderBy('name')->lists('name', 'id');

                $data = view('apointments.partials.ajax_select',compact('tasks'))->render();
                return response()->json(['options'=>$data]);
            }
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $categories = Category::where('finished_at', '=', null)->where('company_id', Auth::user()->company_id)->orderBy('name')->lists('name', 'id')->all();
            $tasks = Task::orderBy('name')->lists('name', 'id')->all();
            return view('apointments.create', compact('categories', 'tasks'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(ApointmentRequest $request)
        {
            $inputs = $request->except(['start', 'end']);
            //$inputs['company_id'] = Auth::user()->company->id; // Relacionamento
            $inputs['company_id'] = Auth::user()->company_id;
            $inputs['user_id'] = Auth::user()->id;

            $startCarbon = Carbon::createFromFormat('d/m/Y H:i', $request['start']);
            if($request['end'] != null){
                $endCarbon = Carbon::createFromFormat('d/m/Y H:i', $request['end']);
                $inputs['end'] = $endCarbon;
            }

            $inputs['start'] = $startCarbon;

            if(isset($inputs['end']) &&$inputs['start']->gt($inputs['end']) ){
                return redirect()->route("apointment.create")->with('error','Data de início do apontamento dever ser menor que a de término.');
            }

            Apointment::create($inputs);

            return redirect()->route('apointment.index');
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $apointment = Apointment::find($id);
            $categories = Category::all()->where('company_id', Auth::user()->company_id)->lists('name', 'id')->all();
            $tasks = Task::where('category_id', $apointment->category_id)->lists('name', 'id')->all();
            return view('apointments.edit', compact('apointment', 'categories', 'tasks'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $apointment = Apointment::findOrFail($id);
            
            $inputs = $request->except(['start', 'end']);
            //$inputs['company_id'] = Auth::user()->company->id; // Relacionamento
            $inputs['company_id'] = Auth::user()->company_id;
            $inputs['user_id'] = Auth::user()->id;

            $startCarbon = Carbon::createFromFormat('d/m/Y H:i', $request['start']);
            if($request['end'] == '')
                $endCarbon = null;
            else
                $endCarbon = Carbon::createFromFormat('d/m/Y H:i', $request['end']);
            $inputs['start'] = $startCarbon;
            $inputs['end'] = $endCarbon;

            if(isset($inputs['end']) && $inputs['start']->gt($inputs['end']) ){
                return redirect()->route("apointment.edit", ['id' => $apointment->id])->with('error','Data de início do apontamento dever ser menor que a de término.');
            }

            $apointment->fill($inputs);
            $apointment->update();

            if(isset($inputs['end']) && $this->greater_than_eight_hours($inputs['start'],$inputs['end'])){
                return redirect()->route("apointment.edit", ['id' => $apointment->id])->with('warning','O apontamento está com mais de 8h.');
            }
            return redirect()->route('apointment.index');
            
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(Request $request)
        {
             if(Auth::user()->id != $request->user_id){
                 return response(['message' => 'Unauthorized'], 401);
             }
            $status = Apointment::destroy($request->id);
            return response()->json(['status' => $status]);
        }

        /**
         * Carrega view de relatório
         */
        public function report(Request $request, $type)
        {
            if($type ==="categorias") return ApointmentController::reportCategory($request);
            else if($type ==="colaboradores") return ApointmentController::reportUser($request);
            else return redirect()->back();
        }


        public function reportUser(Request $request){
            $users = User::where('company_id', Auth::user()->company_id)->orderBy('name')->get()->lists('name', 'id')->all();
            

            $categories = Category::withTrashed()->where('company_id', Auth::user()->company_id)->orderBy('name')->lists('name', 'id')->all();
            $tasks = array();

            $dados = $request->request->all();

            $apointments = null;
            $xAxis = array();
            $points = array();
            $workload = array();
            $workload_aux = array();
            $range = 7;
            $end_ = Carbon::now();
            $user_selected = null;
            $category_selected = '';
            $task_selected = '';
            $date_range = "";

            $apointments = Apointment::where('company_id', Auth::user()->company_id);
                                         
            collect($apointments)->sortByDesc('start');

                                        

            // Verifica seleção de usuário
            if(isset($dados['user_id'])  && !empty ( $dados['user_id'] ))
                $user_selected = $dados['user_id'];

            // Verifica seleção de data
            if(isset($dados['date_range']) && !empty ( $dados['date_range'] )){
                $date_range = $dados['date_range'];
                $dateRArray = explode(' - ', $dados['date_range']);



                $dateStart = Carbon::createFromFormat('d/m/Y', $dateRArray[0])->startOfDay();
                $dateEnd = Carbon::createFromFormat('d/m/Y', $dateRArray[1])->endOfDay();

                $range = $dateEnd->diffInDays($dateStart); // Quantidade de dias em int
                
                $end_ = $dateEnd; // Carbon date object

                $apointments->where('start', '>', $dateStart)
                                ->where('end', '<=', $dateEnd);

            }


            // Verifica seleção de usuário
            if(isset($dados['user_id']) && !empty ( $dados['user_id'] )){
                $apointments = $apointments->where('user_id', $dados['user_id']);
            }
            if(isset($dados['category_id']) && !empty ( $dados['category_id'] )){
                $category_selected = $dados['category_id'];
                if(isset($dados['task_id']) && !empty ( $dados['task_id'] )){
                    $task_selected = $dados['task_id'];
                    $tasks = Task::where('category_id', $category_selected)->get()->lists('name', 'id')->all();
                    $apointments = $apointments->where('task_id', $dados['task_id']);
                }
                $apointments = $apointments->where('category_id', $dados['category_id']);
            }

            if($request->isMethod('GET')){
                return view('apointments.reportUserEmpty', compact('users', 'categories', 'tasks', 'user_selected'));
            }

            DB::enableQueryLog();

            $aux1 = $end_;
            $aux2 = $end_;
            $aux3 = $end_;
            $aux4 = $end_;

            $apointments = $apointments->get()->all();

            for($i = $range; $i>=0 ; $i--) {
                $aux = clone $dateEnd;
                $valor = $aux->subDays($i)->format('d/m');
                array_push($xAxis, $valor);
                $points[$valor] = 0;
                $workload[$valor] = 0;
                $workload_aux[$valor] = [];
            }
            
            
            $tableData = [];

            $aux_min = 0;
            foreach ($apointments as $apointment) {
                $apoint = $apointment['attributes'];
                $end = $apoint['end'];
                
                if($end != null) {

                    $dateStart = Carbon::parse($apoint['start']);
                    $dateEnd = Carbon::parse($apoint['end']);
                    $hours = $dateEnd->diffInMinutes($dateStart);
                    
                    $points[$dateStart->format('d/m')] += $hours;
                    $user = $apointment->user;
                    if(!in_array($user->id, $workload_aux[$dateStart->format('d/m')])){
                        array_push($workload_aux[$dateStart->format('d/m')], $user->id);
                        $workload[$dateStart->format('d/m')] += $user->CH;
                    }
                }
                $min = $apointment->end->diffInMinutes($apointment->start);
                $aux_min+=$min;
                array_push($tableData, [
                    'nome' => User::find($apointment->user_id)->name,
                    'categoria' => Category::withTrashed()->find($apointment->category_id)->name,
                    'tarefa' => Task::find($apointment->task_id)->name,
                    'entrada' => $apointment->start->format('d/m/Y - H:i:s'),
                    'saida' => $apointment->end->format('d/m/Y - H:i:s'),
                    'duracao' => (floor($min/60) < 10?('0'.floor($min/60)):floor($min/60)).":".(($min%60) < 10?('0'.$min%60):($min%60)).':00',
                    'update' => $apointment->updated_at->format('d/m/Y - H:i:s')
                    
                    ]);
                   
                
            }
                      
            $totalCH = 0;
            $chart1 = array();
            $line = array();
            foreach ($points as $point) {
                array_push($chart1, round($point/60, 2));
                $totalCH += $point;
            }

            foreach($workload as $day){
                array_push($line, $day);
            }
            
            $total_worked_hours = floor($totalCH/60);
            $remaining_minutes = ($totalCH%60);
            
            $xAxis = json_encode($xAxis);
            $chart1 = json_encode($chart1);
            $line  = json_encode($line);
          

            // array_sort($tableData, 'entrada', SORT_DESC);
           
            return view('apointments.reportUser',
                [
                    'categories' => $categories,
                    'users' => $users,
                    'tasks' => $tasks,
                    'xAxis' => $xAxis,
                    'chart1' => $chart1,
                    'user_selected' => $user_selected,
                    'category_selected' => $category_selected,
                    'task_selected' => $task_selected,
                    'date_range' => $date_range,
                    'range' => $range,
                    'total_worked_hours' => $total_worked_hours,
                    'remaining_minutes' => $remaining_minutes,
                    'tableData' =>  $tableData,
                    'line' => $line
                ]
            );
        }

        public function reportCategory(Request $request){
            $users = User::where('company_id', Auth::user()->company_id)->orderBy('name')->get()->lists('name', 'id')->all();
            
            $categories = Category::withTrashed()->where('company_id', Auth::user()->company_id)->orderBy('name')->lists('name', 'id')->all();
            $tasks = array();

            $dados = $request->request->all();

            $apointments = null;
            $xAxis = array();
            $points = array();
            $workload = array();
            $workload_aux = array();
            $range = 7;
            $end_ = Carbon::now();
            $user_selected = null;
            $category_selected = '';
            $task_selected = '';
            $date_range = "";

            $query = \DB::table('apointments')
                            ->select(\DB::raw('
                                tasks.id as id, tasks.name as name, 
                                tasks.planned_hours, apointments.start as start, 
                                apointments.end as end, categories.name as category_name, 
                                categories.id as category_id,
                                users.name as user_name'
                            ))
                            ->where('apointments.company_id', Auth::user()->company_id);

            // Verifica seleção de usuário
            if(isset($dados['user_id'])  && !empty ( $dados['user_id'] ))
                $user_selected = $dados['user_id'];

            // Verifica seleção de data
            if(isset($dados['date_range']) && !empty ( $dados['date_range'] )){
                $date_range = $dados['date_range'];
                $dateRArray = explode(' - ', $dados['date_range']);

                $dateStart = Carbon::createFromFormat('d/m/Y', $dateRArray[0])->startOfDay();
                $dateEnd = Carbon::createFromFormat('d/m/Y', $dateRArray[1])->endOfDay();

                $range = $dateEnd->diffInDays($dateStart); // Quantidade de dias em int
                
                $end_ = $dateEnd; // Carbon date object

                $query->where('apointments.start', '>', $dateStart)
                                ->where('apointments.end', '<=', $dateEnd);

            }


            // Verifica seleção de usuário
            if(isset($dados['user_id']) && !empty ( $dados['user_id'] )){
                $query = $query->where('apointments.user_id', $dados['user_id']);
            }
            
            if(isset($dados['category_id']) && !empty ( $dados['category_id'] )){
                $category_selected = $dados['category_id'];
                if(isset($dados['task_id']) && !empty ( $dados['task_id'] )){
                    $task_selected = $dados['task_id'];
                    $tasks = Task::where('category_id', $category_selected)->get()->lists('name', 'id')->all();
                    $query = $query->where('apointments.task_id', $dados['task_id']);
                }
                $query = $query->where('apointments.category_id', $dados['category_id']);
            }

            if($request->isMethod('GET')){
                return view('apointments.reportCategoryEmpty', compact('users', 'categories', 'tasks', 'user_selected'));
            }

            DB::enableQueryLog();

            $aux1 = $end_;
            $aux2 = $end_;
            $aux3 = $end_;
            $aux4 = $end_;

            $query = $query
                        ->join('tasks','tasks.id','=','apointments.task_id')
                        ->join('categories','categories.id','=','tasks.category_id')
                        ->join('users', 'users.id', '=', 'apointments.user_id')
                        ->orderBy('apointments.task_id', 'tasks.category_id')
                        ->get();

            $tableData = [];
            $tasks_workload = [];
            $tasks_name = [];
            $tasks_planned_workload = [];
            $task_id = -1;
            $i = -1;
            $total_min = 0;
            foreach($query as $task){
                if($task_id != $task->id){
                    $i++; 
                    $task_id = $task->id;
                    $total_task_min = 0;
                }
                $dateStart = Carbon::parse($task->start);
                $dateEnd = Carbon::parse($task->end);
                $min = $dateEnd->diffInMinutes($dateStart);
                $total_min+=$min;
                $total_task_min+=$min;
                if(isset($tasks_workload[$i])){
                    $tasks_workload[$i] += round($min/60,2);
                }else{
                    $tasks_workload[$i] = round($min/60,2);
                    $tasks_name[$i] = $task->name;
                    $tasks_planned_workload[$i] = $task->planned_hours;  
                    $tableData[$i]['name'] = $tasks_name[$i];
                    $tableData[$i]['planned_workload'] = ($tasks_planned_workload[$i] < 20?'0'.$tasks_planned_workload[$i]:$tasks_planned_workload[$i]).":00:00";
                    $tableData[$i]['category'] = $task->category_name;
                    $tableData[$i]['users'] = [];
                }
                if(!in_array($task->user_name,$tableData[$i]['users']))
                    array_push($tableData[$i]['users'],$task->user_name);
                $tableData[$i]['workload'] = (floor($total_task_min/60) < 10?('0'.floor($total_task_min/60)):floor($total_task_min/60)).":".(($total_task_min%60) < 10?('0'.$total_task_min%60):($total_task_min%60)).':00';
                
            }            
            
            $xAxis = json_encode($tasks_name);
            $workload = json_encode($tasks_workload);
            $planned_workload  = json_encode($tasks_planned_workload);
            $total_worked_hours = floor($total_min/60)."h".($total_min%60)."m";
            return view('apointments.reportCategory',
                [
                    'categories' => $categories,
                    'users' => $users,
                    'tasks' => $tasks,
                    'xAxis' => $xAxis,
                    'workload' => $workload,
                    'planned_workload' => $planned_workload,
                    'user_selected' => $user_selected,
                    'category_selected' => $category_selected,
                    'task_selected' => $task_selected,
                    'date_range' => $date_range,
                    'range' => $range,
                    'total_worked_hours' => $total_worked_hours,
                    // 'remaining_minutes' => $remaining_minutes,
                    'tableData' => $tableData,
                ]
            );
        }
        
        /**
         * Encerra apontamento
         *
         * @return \Illuminate\Http\Response
         */
        public function check(Request $request)
        {
            $id = $request->id;

            $apointment = Apointment::findOrFail($id);
            $inputs['end'] = Carbon::now();
            $inputs['comment'] = $request->comment;

            if($apointment->start->gt($inputs['end']) ){
                return response()->json([
                    'success' => false,
                    'message'=> 'Data de início deve ser menor que a de término.'
                    ]);
            }

            $apointment->fill($inputs);
            

            $worked_hours = $this->calcula_diferenca_horas($apointment['start'], $apointment['end']);

            if($this->greater_than_eight_hours($apointment['start'], $apointment['end']) && !isset($request->gt_eight_hours)){
                return response()->json([
                    'success' => false,
                    'gt_eight_hours' => true,
                    'status' => $worked_hours
                    ]);
            }
            $status = $apointment->update();
            return response()->json([
                'success'=> true,
                'status' => $worked_hours
                ]);
        }

        public function hour_storage(){
            // Lógica pendente, para evitar crash, retorna para home
            Flash::error("Página não encontrada!");
            return redirect(route('home'));
        }

        /**
         * Encerra apontamento
         *
         * @return \Illuminate\Http\Response
         */
        public function check_last(Request $request)
        {
            //dd('sss', $request->info);


            $data = Apointment::where('user_id', Auth::user()->id)->where('end', null)->orderBy('start', 'DESC')->first();
            if($data == null)
                return response()->json(['status' => null, 'message' =>'Não existe apontamento ativo.']);
            
            $id = $data->id;

            $apointment = Apointment::findOrFail($id);            
            $inputs['end'] = Carbon::now();
            if($apointment->start->gt($inputs['end']) ){
                return response()->json([
                    'success' => false,
                    'message'=> 'Data de início deve ser menor que a de término.'
                    ]);
            }
            $inputs['comment'] = (empty($data->comment)) ? $request->info[0] : $data->comment;
            $apointment->fill($inputs);
            

            $worked_hours = $this->calcula_diferenca_horas($apointment['start'], $apointment['end']);

            if($this->greater_than_eight_hours($apointment['start'], $apointment['end']) && !isset($request->gt_eight_hours)){
                return response()->json([
                    'success' => false,
                    'gt_eight_hours' => true,
                    'status' => $worked_hours
                    ]);
            }

            $percentage = floatval($request->info[1]);
            if(isset($percentage)){
                $task = Task::find($data->task_id);
                $task->percentage = $percentage;
                $task->save();
            }
            $status = $apointment->update();
            return response()->json([
                'success' => true,
                'status' => $worked_hours
            ]);
        }

        public function computar(Request $request)
        {
            $current = Apointment::where('user_id', Auth::user()->id)->where('end', null)->orderBy('start', 'DESC')->first();
            $now = Carbon::now();

            if($current != null){
                $aux['end'] = $now;
                $current->fill($aux);
                $current->update();
            }
            
            $status = $now->format('d/m/Y H\hi\m\i\n');

            $inputs['category_id'] = $request->category_id;
            $inputs['task_id'] = $request->task_id;
            
            $inputs['company_id'] = Auth::user()->company_id;
            $inputs['user_id'] = Auth::user()->id;

            $inputs['start'] = $now;

            Apointment::create($inputs);

            return response()->json(['status' => $status]);
        }

        private function calcula_diferenca_horas($start, $end)
        {
            $dateStart = Carbon::parse($start);
            $dateEnd = Carbon::parse($end);

            $diff = $dateEnd->diffInMinutes($dateStart);
            $hours = floor($diff/60) . 'h' . ($diff%60) . 'min';

            return $hours;         
        }

        private function greater_than_eight_hours($start, $end)
        {
            $dateStart = Carbon::parse($start);
            $dateEnd = Carbon::parse($end);

            $diff = $dateEnd->diffInMinutes($dateStart);

            if(floor($diff/60) > 8)
                return true;
            
            return false;         
        }

    }   


    ?>