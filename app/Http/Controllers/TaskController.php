<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use Auth;
use DB;

use App\Category;
use App\Task;
use App\User;
use App\Apointment;
use Carbon\Carbon;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tasks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->where('company_id', Auth::user()->company_id)->lists('name', 'id')->all();
        $tasks = Task::all()->lists('name', 'id')->all();
        return view('tasks.create',compact('categories', 'tasks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $inputs = $request->all();
            $ddlCarbon = Carbon::createFromFormat('d/m/Y H:i', $request['deadline']);
            $inputs['deadline'] = $ddlCarbon;

            $c = Task::create($inputs);
            return redirect()->route('task.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        $category = Category::find($task->category_id);
        return view('tasks.show', compact('task', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        $categories = Category::all()->where('company_id', Auth::user()->company_id)->lists('name', 'id')->all();
        $tasks = Task::where('category_id', $task->category_id)->lists('name', 'id')->all();
        return view('tasks.edit', compact('task', 'categories', 'tasks'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $inputs = $request->all();
        $ddlCarbon = Carbon::createFromFormat('d/m/Y H:i', $request['deadline']);
        $inputs['deadline'] = $ddlCarbon;
        $inputs['planned_hours'] = intval($inputs['planned_hours']);
        $task->fill($inputs);
        $task->update();
        return redirect()->route('task.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $apointments = Apointment::where('task_id', $request->id)->delete();
        $status = Task::destroy($request['id']);     
        return response()->json(['status' => $status]);
    }

    public function ajax_tasks(){
        
        $array_data['data'] = [];
        
        // Filtra as tarefas apenas pertencentes à empresa do usuário
        $tasks = Task::join('categories', 'categories.id', '=', 'tasks.category_id')
                    ->where('company_id', Auth::user()->company_id)
                    ->select('tasks.*')
                    ->get();
        foreach($tasks as $task){
            $apointments = Apointment::where('task_id', $task->id)->where('company_id', Auth::user()->company_id)->get();
            $hours = 0;
            foreach($apointments as $ap){
                $hours += (Carbon::parse($ap->end)->diffInHours(Carbon::parse($ap->start)));
            }
            array_push($array_data['data'], [
                'idTask' => $task->id,
                'hours' => ($task->planned_hours == null ? "0" : strval($task->planned_hours))."/".$hours,
                'task' => $task->name,
                'category' => Category::withTrashed()->find($task->category_id)->name,
                'percentage' => $task->percentage,
                'deadline' => Carbon::parse($task->deadline)->format('d/m/Y H:i')
            ]);
        }

        return json_encode($array_data, JSON_UNESCAPED_SLASHES);
    }
    
}
