<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Task;
use Auth;

use Carbon\Carbon;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories_for_table = Category::where('finished_at', '=', null)
        ->where('company_id',Auth::user()->company_id)
        ->get();
        foreach($categories_for_table as $category){
            $tasksNumber = Task::where('category_id', '=', $category->id)->count();
            $category->tasksNumber = $tasksNumber;
            
        }
        $categories_for_finished_table = Category::where('finished_at', '!=', '')->get();
        foreach($categories_for_finished_table as $category){
            $tasksNumber = Task::where('category_id', '=', $category->id)->count();
            $category->tasksNumber = $tasksNumber;
        }
        return view('categories.index', compact('categories_for_table', 'categories_for_finished_table'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $category = new Category;
        $category->name = $request->name;
        $category->HH = $request->HH;
        $category->company_id = Auth::user()->company->id;
        $category->save();
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->HH = $request->HH;
        $category->save();
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('category.index');
    }

    /**
     * Inverte o estado de finalização do projeto (se estiver ativo vira finalizado e vice-versa)
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function invertFinished(Request $request)
    {        
        $category = Category::find($request->id);
        if(!$category->finished_at) {
            $category->finished_at = Carbon::now();
        } else {
            $category->finished_at = null;
        }        
        $category->save();
        return response()->json(array());
    }
}
