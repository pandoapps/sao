<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Manager
{

    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasRole(['manager'])) {
            return redirect()->back();
        }

        return $next($request);
    }
}
