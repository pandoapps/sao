<?php

namespace App\Http\Middleware;

use Closure;
use Flash;
use Auth;
use Carbon\Carbon;

class ExpireMiddleware
{    
    public function handle($request, Closure $next)
    {
        if (Auth::user()->company) {
            $now = Carbon::now(); //cria uma instância do Carbon com a data atual no timezone que está configurado seu PHP
            $remainingDays = ($now->diffInDays(Auth::user()->company->expired_at));
            if (Auth::user()->company->expired_at == NULL) {
                $request['time'] = 100;
                $request['days'] = 0;
                return $next($request);
            } elseif ($remainingDays > 0) {
                $isValid = true;
            } else {
                $isValid = false;
            }
        }       
        elseif (Auth::user()->company == NULL) {
            $request['time'] = 100;
            $request['days'] = 0;
            return $next($request);
        }
        if ($isValid) {        
            $request['time'] = $remainingDays * (10 / 3);
            $request['days'] = $remainingDays;
            return $next($request);
        } else {
            Auth::logout();
            Flash::error("Seu tempo expirou");
            return redirect('/pagamento');
            
        }
        
        
    }
}