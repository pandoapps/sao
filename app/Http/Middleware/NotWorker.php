<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class NotWorker
{

    public function handle($request, Closure $next)
    {
        if (Auth::user()->hasRole(['worker'])) {
            return redirect()->back();
        }

        return $next($request);
    }
}
