<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['auth', 'expire']], function() {
	
	Route::get('/ajax_apointments', ['as' => 'apointment.ajax_apointments',    'uses' => 'ApointmentController@ajax_apointments']);
	Route::get('/ajax_select_categories',     ['as'=>'apointments.partials.ajax_select_categories', 'uses'=>'ApointmentController@ajax_select_categories']);		
	Route::post('/ajax_select',     ['as'=>'apointments.partials.ajax_select', 'uses'=>'ApointmentController@ajax_select']);
	Route::get('/ajax_tasks',       ['as' => 'task.ajax_tasks',                'uses' => 'TaskController@ajax_tasks']);

	Route::get('/',                 ['as' => 'apointment.index',               'uses' => 'ApointmentController@index']);
	Route::get('/home',             ['as'=>'home'              ,               'uses' => 'ApointmentController@index']);

	Route::group(['prefix' => 'tasks'], function() {
		Route::get('/',               ['as' => 'task.index',   'uses' => 'TaskController@index']);
		Route::get('/criar',          ['as' => 'task.create',  'uses' => 'TaskController@create']);
		Route::get('/editar/{id}',    ['as' => 'task.edit',    'uses' => 'TaskController@edit']);
		Route::put('/atualizar/{id}', ['as' => 'task.update',  'uses' => 'TaskController@update']);
		Route::post('/deletar',       ['as' => 'task.destroy', 'uses' => 'TaskController@destroy']);
		Route::post('/store',         ['as' => 'task.store',   'uses' => 'TaskController@store']);
		Route::get('{id}',            ['as' => 'task.show',    'uses' => 'TaskController@show']);
	});

	Route::group(['prefix' => 'apontamentos'], function() {
		Route::get('/criar',              ['as' => 'apointment.create',         'uses' => 'ApointmentController@create']);
		Route::post('/store',             ['as' => 'apointment.store',          'uses' => 'ApointmentController@store']);
		Route::post('/deletar',           ['as' => 'apointment.destroy',        'uses' => 'ApointmentController@destroy']);
		Route::put('/atualizar/{id}',     ['as' => 'apointment.update',         'uses' => 'ApointmentController@update']);
		Route::get('/',                   ['as' => 'apointment.index',          'uses' => 'ApointmentController@index']);
		Route::get('/editar/{id}',        ['as' => 'apointment.edit',           'uses' => 'ApointmentController@edit']);
		Route::post('/encerrar/{id}',     ['as' => 'apointment.check',          'uses' => 'ApointmentController@check']);
		Route::post('/encerrar_ultimo',   ['as' => 'apointment.check_last',     'uses' => 'ApointmentController@check_last']);
		Route::get('/ativos',             ['as' => 'apointment.ativos',         'uses' => 'ApointmentController@ativos']);
		Route::get('/ajax_ativos',        ['as' => 'apointment.ajax_ativos',    'uses' => 'ApointmentController@ajax_ativos']);

		Route::group(['middleware' => ['permission:see-reports']], function(){
			Route::get('/relatorios/{type}',  ['as' => 'apointment.relatorio',      'uses' => 'ApointmentController@report']);
			Route::post('/relatorios/{type}', ['as' => 'apointment.relatorio',      'uses' => 'ApointmentController@report']);
		});

		Route::get('/banco_de_horas',     ['as' => 'apointment.hour_storage',   'uses' => 'ApointmentController@hour_storage']);
		Route::post('/computar_horas',    ['as' => 'apointment.computar_horas', 'uses' => 'ApointmentController@computar']);
	});

	Route::group(['prefix' => 'usuarios', 'middleware' => ['permission:manage-users']], function(){
		Route::get('/',             ['as' => 'user.index',   'uses' => 'UserController@index']);
		Route::get('/criar',        ['as' => 'user.create',  'uses' => 'UserController@create']);
		Route::post('/criar',       ['as' => 'user.store',   'uses' => 'UserController@store']);
		Route::get('/editar/{id}',  ['as' => 'user.edit',    'uses' => 'UserController@edit']);
		Route::put('/editar/{id}',  ['as' => 'user.update',  'uses' => 'UserController@update']);
		Route::post('/deletar/{id}', ['as' => 'user.destroy', 'uses' => 'UserController@destroy']);
	});
	Route::group(['prefix' => 'company', 'middleware' => ['permission:manage-company']], function(){
		Route::get('/',             ['as' => 'company.index',   'uses' => 'CompanyController@index']);
		Route::get('/criar',        ['as' => 'company.create',  'uses' => 'CompanyController@create']);
		Route::post('/criar',       ['as' => 'company.store',   'uses' => 'CompanyController@store']);
		Route::get('/editar/{id}',  ['as' => 'company.edit',    'uses' => 'CompanyController@edit']);
		Route::put('/editar/{id}',  ['as' => 'company.update',  'uses' => 'CompanyController@update']);
		Route::get('/deletar/{id}', ['as' => 'company.destroy', 'uses' => 'CompanyController@destroy']);
	});

	Route::group(['prefix' => 'perfis'], function(){
		Route::get('/',             ['as' => 'role.index',   'uses' => 'RoleController@index']);
		Route::get('/criar',        ['as' => 'role.create',  'uses' => 'RoleController@create']);
		Route::post('/criar',       ['as' => 'role.store',   'uses' => 'RoleController@store']);
		Route::get('/editar/{id}',  ['as' => 'role.edit',    'uses' => 'RoleController@edit']);
		Route::put('/editar/{id}',  ['as' => 'role.update',  'uses' => 'RoleController@update']);
		Route::get('/deletar/{id}', ['as' => 'role.destroy', 'uses' => 'RoleController@destroy']);
	});

	Route::group(['prefix' => 'categorias'], function(){
		Route::get('/',             ['as' => 'category.index',   'uses' => 'CategoryController@index']);
		Route::get('/criar',        ['as' => 'category.create',  'uses' => 'CategoryController@create']);
		Route::post('/criar',       ['as' => 'category.store',   'uses' => 'CategoryController@store']);
		Route::get('/editar/{id}',  ['as' => 'category.edit',    'uses' => 'CategoryController@edit']);
		Route::put('/editar/{id}',  ['as' => 'category.update',  'uses' => 'CategoryController@update']);
		Route::get('/deletar/{id}', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);
		// Rota para marcar o projeto como finalizado
		Route::post('/finalizar/',  ['as' => 'category.invertFinished', 'uses' => 'CategoryController@invertFinished']);
	});

	Route::group(['prefix' => 'gasto', 'middleware' => ['permission:see-financial-reports']], function(){
		Route::get('/',             ['as' => 'cost.index',   'uses' => 'CostController@index']);
		Route::get('/criar',        ['as' => 'cost.create',  'uses' => 'CostController@create']);
		Route::post('/criar',       ['as' => 'cost.store',   'uses' => 'CostController@store']);
		Route::get('/editar/{id}',  ['as' => 'cost.edit',    'uses' => 'CostController@edit']);
		Route::put('/editar/{id}',  ['as' => 'cost.update',  'uses' => 'CostController@update']);
		Route::get('/deletar/{id}', ['as' => 'cost.destroy', 'uses' => 'CostController@destroy']);

		// Rota dos relatórios financeiros
		Route::get('/relatorios/',  ['as' => 'cost.relatorio', 'uses' => 'CostController@report']);
		Route::post('/relatorios/', ['as' => 'cost.relatorio', 'uses' => 'CostController@report']);
	});
});

Route::group(['prefix' => 'pagamentos'], function(){
	Route::get('/',             ['as' => 'payment.index',   'uses' => 'PaymentsController@index']);
	Route::get('/criar',        ['as' => 'payment.create',  'uses' => 'PaymentsController@create']);
	Route::post('/criar',       ['as' => 'payment.store',   'uses' => 'PaymentsController@store']);
	Route::get('/editar/{id}',  ['as' => 'payment.edit',    'uses' => 'PaymentsController@edit']);
	Route::put('/editar/{id}',  ['as' => 'payment.update',  'uses' => 'PaymentsController@update']);
	Route::get('/deletar/{id}', ['as' => 'payment.destroy', 'uses' => 'PaymentsController@destroy']);
});

Route::get('auth/login',  ['as' => 'login-form', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'login',      'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'logout',     'uses' => 'Auth\AuthController@getLogout']);

// Rotas landing page
Route::group(['prefix' => 'landing'], function(){
	Route::get('/',  ['as' => 'home', 'uses' => 'LandingController@index']);
	Route::post('/ajax_register', ['as' => 'landing.ajax_register', 'uses' => 'LandingController@register']);
});

?>