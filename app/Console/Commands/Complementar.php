<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use App\Apointment;
use App\Category;
use App\Task;
use App\User;
use Carbon\Carbon;

class Complementar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'complementar:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Complementar horas restantes do usuario para outra tarefa';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // seleciona todos usuarios
        $users = User::all();
        // pega o horario do momento
        $now = Carbon::now();
        $total_worked = 0;

        foreach ($users as $user) {
            
            $aponts = Apointment::where('user_id', $user->id)->get();
            
            foreach ($aponts as $apont) {
            $created = $apont->created_at; 
            $diference = $created->diffInDays($now); 
            // seleciona somente os apontamentos do dia  
                if($diference == 0){
                $start = $apont->start;
                $end = $apont->end;
                $time = $start->diffInMinutes($end);    
                }
                $total_worked += $time;

            }
            $total_worked_hours = floor($total_worked/60);
            $timecomplement = $total_worked_hours->diffInHours($user->CH);
            
        //  se estiver horas sobrando para serem cumpridas no dia
            if($timecomplement>0){
                $start_apont = Carbon::now();
                $end_apont = $start_apont->addHours($timecomplement);
                $task_complement_id = $user->task_sub;
                $category_complement_id = $user->category_sub;
                $values = array('user_id' => $user->id,'task_id' => $task_complement_id,'company_id' => $user->company_id,'category_id' => $category_complement_id,'name' => '','start' => $start_apont,'end' => $end_apont);
                Apointment::insert($values);
            }

        
        }
        
       
       
            
    }
}