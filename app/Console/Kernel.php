<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Apointment;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $openApointments = Apointment::where('end', null)->get();
            
            if(isset($openApointments)){
                foreach ($openApointments as $openApointment) {

                    if(!$openApointment->user->CH) continue;

                    $apointmentDate = Carbon::parse($openApointment->start);
                    $otherApointments = Apointment::where('user_id', $openApointment->user_id)
                                                    ->whereNotNull('end')
                                                    ->whereDate('start','=',$apointmentDate->toDateString())
                                                    ->get();
                    $worked_hours = 0;

                    foreach ($otherApointments as $apointment) {
                        $dateStart = Carbon::parse($apointment->start);
                        $dateEnd = Carbon::parse($apointment->end);
                        $worked_hours += $dateEnd->diffInMinutes($dateStart);
                    }

                    $remaining_work = $openApointment->user->CH*60 - $worked_hours;
                    $apointmentDate->addMinutes($remaining_work > 0?$remaining_work:0);
                    $openApointment->end = $apointmentDate;
                    $openApointment->comment = "Apontamento fechado automaticamente.";
                    $openApointment->save();
                }

            }
        })->daily();


        $schedule->command('complementar:users')
        ->dailyAt('23:58');
    }
}
