<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Cost extends Model
{
    protected $fillable = [
        'name', 'description','type', 'value', 'costable_id', 'costable_type', 'start', 'end'
    ];

    protected $dates = ['start', 'end'];

    public function costable()
    {
        return $this->morphTo();
    }

    public function getTypeNameAttribute(){
        
        $dictionary = [
            'variable' => 'Variável',
            'fixed' =>  'Fixo'
        ];

        return $dictionary[$this->type];
    }

    public function getFormattedStartAttribute(){
        return Carbon::parse($this->start)->format('d/m/Y');  
    }

    public function getFormattedEndAttribute(){
        if(!$this->end) null;
        return Carbon::parse($this->end)->format('d/m/Y');  

    }

    /**
     * Always show value with 2 decimal places and ',' as decimal separator
     * 
     */
    public function getValueAttribute($value)
    {
        return number_format($value*-1, 2, ',', '.');
    }

    /**
     * Always save value with '.' as decimal separator and negative
     * 
     */
    public function setValueAttribute($value)
    {
        $value = str_replace('.', '', $value);        
        $value = str_replace(',', '.', $value);

        return $this->attributes['value'] = $value*-1;
    }

}
