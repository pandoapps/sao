<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'company_id', 'HH'];

    public function costs()
    {
        return $this->morphMany('App\Cost', 'costable');
    }

    /**
     * Always show HH with 2 decimal places and ',' as decimal separator
     * 
     */
    public function getHHAttribute($value)
    {
        // number_format(number,decimals,decimalpoint,thousandseparator) 
        return number_format($value, 2, ',', ' ');
    }

    /**
     * Always save HH with '.' as decimal separator and removes all spaces
     * 
     */
    public function setHHAttribute($value)
    {
        $value = str_replace(' ', '', $value);
        $value = str_replace(',', '.', $value);

        return $this->attributes['HH'] = $value;
    }
}
