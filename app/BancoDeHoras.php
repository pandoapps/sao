<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BancoDeHoras extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banco_de_horas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'start', 'end', 'company_id'];
}
