<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBancoDeHorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banco_de_horas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('start');
            $table->timestamp('end');
            $table->integer('company_id')->unsigned()->index();
            $table->foreign('company_id')
                ->references('id')->on('companies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banco_de_horas');
    }
}
