<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
	private $table = 'categories';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArray = [
    	['id' => 1, 'name' => 'DEV', 'company_id' => '1'],
        ['id' => 2, 'name' => 'Comercial', 'company_id' => '1'],
    	['id' => 3, 'name' => 'Administrativo', 'company_id' => '1'],
        ['id' => 4, 'name' => 'FREE TIME', 'company_id' => '1'],
    	];

    	DB::table($this->table)->insert($dataArray);
    }
}
