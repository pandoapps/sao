<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
	private $table = 'users';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArray = [
            ['CH' => '0','HH' => '0','fixed_pay' => '0', 'company_id' => null, 'name' => 'pandoapps',        'email' => 'pandoapps',                   'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            
            # Pando people
            ['CH' => '8', 'HH' => '0', 'fixed_pay' => '500', 'company_id' => '1',  'name' => 'Thiago Ferreira',  'email' => 'thiago@pandoapps.com.br',     'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '0', 'fixed_pay' => '500', 'company_id' => '1',  'name' => 'Felipe Reis',      'email' => 'felipereis@pandoapps.com.br', 'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '10', 'fixed_pay' => '0', 'company_id' => '1',  'name' => 'Airton Motoki',    'email' => 'motoki@pandoapps.com.br',     'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '10', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Thiago Vilasboas', 'email' => 'thiagovb@pandoapps.com.br',   'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '10', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Adan',             'email' => 'adan@pandoapps.com.br',       'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '6', 'HH' => '40', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Rodolfo',          'email' => 'rodolfo@pandoapps.com.br',    'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '10', 'fixed_pay' => '0', 'company_id' => '1',  'name' => 'Breno',            'email' => 'breno@pandoapps.com.br',      'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '10', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Rafael Siqueira',  'email' => 'rafaels@pandoapps.com.br',    'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '30', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Demétrius',        'email' => 'demetrius@pandoapps.com.br',  'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '8', 'HH' => '10', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Iago',             'email' => 'iago@pandoapps.com.br',       'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '4', 'HH' => '20', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Rafael Carvalho',  'email' => 'rafaelc@pandoapps.com.br',    'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
            ['CH' => '4', 'HH' => '10', 'fixed_pay' => '200', 'company_id' => '1',  'name' => 'Tássio',           'email' => 'tassio@pandoapps.com.br',     'password' => bcrypt('123'),'phone' => 'null','role' => 'null'],
        ];

        DB::table($this->table)->insert($dataArray);
    }
}
