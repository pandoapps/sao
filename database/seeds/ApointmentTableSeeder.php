<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class ApointmentTableSeeder extends Seeder
{
    private $table = 'apointments';

    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
    {
        $dataArray = [
            ['user_id'=> 2, 'task_id'=>10, 'company_id'=>1, 'category_id' => 2, 'name'=>"Teste 1", 'start'=>'2018-01-21 8:00:00', 'end'=> '2018-01-21 8:00:00','comment'=>""]
        ];

        DB::table($this->table)->insert($dataArray);
    }
}
