<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    private $table = 'roles';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArray = [
            ['name' => 'admin',   'display_name' => 'Administrador', 'description' => 'Usuário administrador geral do sistema.',    'company_id' => null],
            ['name' => 'manager', 'display_name' => 'Gerente',       'description' => 'Usuário administrador da empresa.',          'company_id' => null],
            ['name' => 'worker',  'display_name' => 'Colaborador',   'description' => 'Funcionários e demais usuários da empresa.', 'company_id' => null],
        ];

        DB::table($this->table)->insert($dataArray);
    }

}
