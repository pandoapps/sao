<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    private $table = 'permissions';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArray = [
            ['id' => 1, 'name' => 'manage-users',           'display_name' => 'Gerenciar Usuários',         'description' => 'Permissão para criar, editar e deletar usuários.'  ],
            ['id' => 2, 'name' => 'manage-roles',           'display_name' => 'Gerenciar Perfis',           'description' => 'Permissão para criar, editar e deletar perfis de usuario.'    ],
            ['id' => 3, 'name' => 'see-reports',            'display_name' => 'Ver Relatórios',             'description' => 'Permissão para acessar relatórios de apontamentos.'],
            ['id' => 4, 'name' => 'see-financial-reports',  'display_name' => 'Ver Relatórios Financeiros', 'description' => 'Permissão para acessar relatórios financeiros.'    ],
            ['id' => 5, 'name' => 'manage-company',         'display_name' => 'Gerenciar Empresas',         'description' => 'Permissão para criar, editar e deletar empresas.'  ],

        ];

        DB::table($this->table)->insert($dataArray);
    }

}