## Objetivo

Esse projeto tem como objetivo facilitar o monitoramento de tempo dos colaboradores de uma empresa de desenvolvimento de software.

## Framework

Desenvolvemos todo o projeto usando [Laravel website](http://laravel.com/docs).

## Setup do ambiente

O ambiente funciona com docker para facilitar a configuração e desenvolvimento. A única coisa que você precisará instalar é o *Docker* e *Docker Compose* citados ali em cima.

* Tem um passo no final da instalação do Docker que é para retirar o "sudo" dos comandos. Faça isso!

Após a instalação, siga os passos abaixo.

Vamos primeiro editar o arquivo hosts do seu computador para termos um dominio em dev.
```
$ sudo nano /etc/hosts
```

Na última linha, coloque:
```
127.0.0.1   dev.sao.com.br
```

Para salvar e sair: CTRL + O -> Enter -> CTRL + X


Agora é hora de inicializar a aplicação, faça o seguinte:
```
$ make setup
```

Aguarde alguns minutinhos e pronto. Sua aplicação estará rodando e acessível em *dev.gerar.com.br*.

Agora só falta migrar o banco:
```
$ make sh
$ php artisan migrate --seed
```

Você configurou seu ambiente Parabéns!!!

## Contributing

Sinta-se convidado a contribuir com o projeto. Issues e linhas de código sempre nos ajudam a melhorar.

## Créditos

Esse projeto foi desenvolvido e é mantido pela [Pandô APPs](http://www.pandoapps.com.br)






